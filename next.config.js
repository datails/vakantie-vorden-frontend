module.exports =
  process.env.NODE_ENV === "development"
    ? {
        trailingSlash: true,
        images: {
          unoptimized: true,
          remotePatterns: [
            {
              protocol: "https",
              hostname: "res.cloudinary.com",
            },
          ],
        },
      }
    : {
        trailingSlash: true,
        output: "export",
        images: {
          unoptimized: true,
          remotePatterns: [
            {
              protocol: "https",
              hostname: "res.cloudinary.com",
            },
          ],
        },
      };
