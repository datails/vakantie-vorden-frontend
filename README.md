# Vakantie Vorden Front-end
A React app that serves the front-end.

## Development
Make sure you have `node 16` running.

Make sure to create a `.env` file locally. Copy it from the `.env.example`.

Next, run:

```bash
npm ci # install dependencies with frozen lockfile
npm run start
```