/** @type {import('ts-jest').JestConfigWithTsJest} */
module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  transform: {
    // add custom transform configurations if needed
    "^.+\\.js$": "babel-jest", // use Babel for JS files
    "^.+\\.tsx?$": "ts-jest", // use ts-jest for TypeScript files
  },
};
