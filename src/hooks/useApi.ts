import { useCallback, useEffect, useMemo, useReducer, useRef } from 'react'

export interface State<T> {
  data?: T
  error?: Error
}

export type Action<T> =
  | { type: 'loading' }
  | { type: 'fetched'; payload: T }
  | { type: 'error'; payload: Error }

export type ApiFunction = (...args: any[]) => Promise<any>

const createInitialState = <T>(): State<T> => ({
  error: undefined,
  data: undefined,
})

const apiReducer = <T>(state: State<T>, action: Action<T>): State<T> => {
  switch (action.type) {
    case 'loading':
      return { ...createInitialState() }
    case 'fetched':
      return { ...createInitialState(), data: action.payload }
    case 'error':
      return { ...createInitialState(), error: action.payload }
    default:
      return state
  }
}

export const useApi = <T>(fn: ApiFunction, args: any[], deps: any[]): State<T> => {
  const [state, dispatch] = useReducer(apiReducer, createInitialState());
  const cancelRequest = useRef<boolean>(false);

  // Memoize args to prevent unnecessary executions if the args haven't changed
  const memoizedArgs = useMemo(() => args, [args]);

  const fetchData = useCallback(async () => {
    if (cancelRequest.current) return;

    dispatch({ type: 'loading' });

    try {
      const data = await fn(...memoizedArgs);
      if (cancelRequest.current) return;

      dispatch({ type: 'fetched', payload: data });
    } catch (error) {
      if (cancelRequest.current) return;

      dispatch({ type: 'error', payload: error as Error });
    }
  }, [fn, memoizedArgs]);

  useEffect(() => {
    cancelRequest.current = false;
    fetchData();

    return () => {
      cancelRequest.current = true;
    };
  }, [fetchData, ...deps]); // Execute the hook whenever `deps` change.

  return state as State<T>;
};
