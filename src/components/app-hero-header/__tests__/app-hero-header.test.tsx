import React from "react";
import { render, screen } from "@testing-library/react";
import AppHeroHeader from "../index";
import "@testing-library/jest-dom";

test("renders AppHeroHeader component with title and background image", () => {
  const img = "de-schuilplaats-douche.jpeg";
  render(
    <AppHeroHeader
      title="Test Title"
      background={img}
      subTitle="Test Subtitle"
    />
  );
  const testTitle = screen.getByText("Test Title");
  const testSubtitle = screen.getByText("Test Subtitle");
  expect(testTitle).toBeInTheDocument();
  expect(testSubtitle).toBeInTheDocument();
  expect(testTitle).toHaveClass(
    "MuiTypography-root makeStyles-title-4 MuiTypography-h2 MuiTypography-gutterBottom MuiTypography-alignCenter"
  );
  expect(testSubtitle).toHaveClass(
    "MuiTypography-root makeStyles-subTitle-5 MuiTypography-h5 MuiTypography-paragraph MuiTypography-alignCenter"
  );
  const containerStyle = screen.getByTestId("hero-container").style;
  expect(containerStyle.backgroundImage).toBe(`url(${img})`);
});
