import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";

export default makeStyles((theme) =>
  createStyles({
    colorWhite: {
      color: "#fff",
    },
    container: {
      minHeight: "450px",
      margin: "-65px 0 0",
      width: "100%",
    },
    content: {
      alignItems: "center",
      // backgroundColor: "rgba(0,0,0, 0.4)",
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      padding: "40px",
    },
    title: {
      color: "#FFF",
      fontFamily:
        "'Comfortaa','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif",
      textTransform: "uppercase",
      fontStyle: "italic",
      fontWeight: 300,
      [theme.breakpoints.down("sm")]: {
        fontSize: "2.2rem",
      },
    },
    subTitle: {
      color: "#FFF",
      fontFamily:
        "'Comfortaa','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif",
      fontSize: "1rem",
      [theme.breakpoints?.down("md")]: {
        display: "none",
      },
    },
  })
);
