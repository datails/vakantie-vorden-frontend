import React from "react";
import makeStyles from "@mui/styles/makeStyles";
import CircularProgress from "@mui/material/CircularProgress";

const useStyles = makeStyles((theme: Record<string, any>) => ({
  root: {
    display: "flex",
    justifyContent: "center",
    alignContent: "center",
    alignItems: "center",
    margin: "80px 0",
    "& > * + *": {
      marginLeft: theme.spacing(2),
    },
  },
}));

function AppLoader() {
  const classes = useStyles();

  return (
    <div className={classes.root} data-testid="app-loader">
      <CircularProgress />
    </div>
  );
}

export default AppLoader;
