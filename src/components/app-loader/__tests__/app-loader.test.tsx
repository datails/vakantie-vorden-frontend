import React from "react";
import { render } from "@testing-library/react";
import AppLoader from "../index";
import "@testing-library/jest-dom";

test("renders AppLoader component", () => {
  const { getByTestId } = render(<AppLoader />);
  const loaderElement = getByTestId("app-loader");
  expect(loaderElement).toBeInTheDocument();
  expect(loaderElement).toHaveClass("makeStyles-root-1");
});
