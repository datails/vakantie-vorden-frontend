import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import AppAccordion from "../index";
import "@testing-library/jest-dom";

test("renders AppAccordion component with default state", () => {
  render(<AppAccordion>Hello World</AppAccordion>);
  expect(screen.getByText("Toon meer")).toBeInTheDocument();
});

test("expands and collapses accordion when clicked", () => {
  render(<AppAccordion>Hello World</AppAccordion>);
  fireEvent.click(screen.getByRole("button"));
  expect(screen.getByText("Toon minder")).toBeInTheDocument();
  fireEvent.click(screen.getByRole("button"));
  expect(screen.getByText("Toon meer")).toBeInTheDocument();
});

test("renders children when expanded", () => {
  render(<AppAccordion>Hello World</AppAccordion>);
  fireEvent.click(screen.getByRole("button"));
  expect(screen.getByText("Hello World")).toBeInTheDocument();
});
