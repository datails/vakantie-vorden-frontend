import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";

export default makeStyles((theme) =>
  createStyles({
    colorWhite: {
      color: "#fff",
    },
    container: {
      "&::before": {
        content: '""',
        display: "block",
        position: "absolute",
        right: "-50%",
        bottom: "-50%",
        width: "200%",
        height: "100%",
        zIndex: 1,
        opacity: 0.2,
        // background: "#FFF",
        transform: "rotate(30deg)",
      },
      minHeight: "70vh",
      margin: "-65px 0 0",
      backgroundImage: `url(${
        require("../../../../assets/de-schuilplaats-vooraanzicht.jpeg").default
          .src
      })`,
      backgroundRepeat: "no-repeat",
      backgroundSize: "cover",
      backgroundAttachment: "fixed",
      backgroundPosition: "center",
      width: "100%",
      position: "relative",
      overflow: "hidden",
      [theme.breakpoints?.down("md")]: {
        minHeight: "50vh",
        backgroundAttachment: "inherit",
      },
    },
    content: {
      position: "relative",
      zIndex: 2,
      alignItems: "center",
      display: "flex",
      textAlign: "left",
      flexDirection: "column",
      marginTop: 60,
      // justifyContent: "center",
      [theme.breakpoints?.down("md")]: {
        marginLeft: "0px",
        maxWidth: "100%",
        textAlign: "center",
      },
    },
    title: {
      color: "#FFF",
      fontFamily:
        "'Comfortaa','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif",
      fontWeight: 300,
      // textTransform: "uppercase",
      fontSize: "2rem",
      fontStyle: "italic",
      marginTop: 60,
      [theme.breakpoints.up("lg")]: {
        fontSize: "3rem",
      },
    },
    subTitle: {
      marginBottom: "25px",
      color: "#FFF",
      fontWeight: 400,
      fontSize: "22px",

      [theme.breakpoints?.down("md")]: {
        display: "none",
      },
    },
    searchBar: {
      background: "rgb(255 255 255 / 90%)",
      borderRadius: "100px",
      padding: "5px 25px",
    },
  })
);
