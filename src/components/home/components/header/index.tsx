import React from "react";
import { Grid, Typography, Paper } from "@mui/material";
import useStyles from "./header.styles";
import { AppSearch } from "../../..";

export default function Header() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Grid container spacing={10} className={classes.container}>
        <Grid item xs={12} sm={12} md={12} className={classes.content}>
          <Typography
            variant="h1"
            component="h1"
            gutterBottom
            className={classes.title}
          >
            B&amp;B De Schuilplaats in Vorden
          </Typography>
          <Typography
            variant="h2"
            component="h2"
            gutterBottom
            className={classes.subTitle}
          >
            Boek &amp; bekijk beschikbaarheid!
          </Typography>
          <Paper className={classes.searchBar}>
            <AppSearch />
          </Paper>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
