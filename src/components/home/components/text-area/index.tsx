import React from "react";
import { AppTextImage } from "../../..";

function TextArea() {
  return (
    <AppTextImage
      title="Bed and Breakfast in Vorden"
      reverse={false}
      text={`Ontdek De Schuilplaats, uw perfecte uitvalsbasis voor een onvergetelijke vakantie in Vorden. Gelegen op een idyllische locatie, ver weg van de drukte, biedt deze Bed and Breakfast de ultieme rust en ontspanning die rustzoekers verlangen. Aan een schilderachtige zandweg vindt u De Schuilplaats, niet alleen nabij de beroemde 8 kastelenroute maar ook op slechts een steenworp afstand van het legendarische Pieterpad. De perfecte locatie voor zowel wandelaars als fietsliefhebbers.\n\nDirect vanuit uw deur stapt u de weelderige tuin in, die leidt naar de eindeloze wandelpaden door het bos. Voor de avontuurlijke bezoekers zijn er uitdagende mountainbikepaden in de nabije omgeving van dit vakantiehuis in Vorden. Zomergasten kunnen genieten van het verkoelende openluchtzwembad ‘In de Dennen’, slechts 8 fietsminuten verwijderd, terwijl de gezellige bebouwde kom van Vorden, met zijn rijke aanbod aan horecagelegenheden, op slechts 2500 meter afstand ligt.\n\nOntdek de charme van de Hanzesteden Zutphen en Deventer, gelegen op respectievelijk 10 en 24 fietskilometers afstand, en dompel uzelf onder in de rijke geschiedenis en cultuur van de regio. Op fietsafstand bevindt zich ook Museum MORE in Kasteel Ruurlo, evenals Bronkhorst, het kleinste stadje van Nederland, beide zijn must-visits voor cultuurliefhebbers.\n\nDe Schuilplaats in Vorden is de ideale keuze voor uw vakantiehuis in Vorden, of u nu op zoek bent naar de rust van de natuur, sportieve activiteiten of culturele uitstapjes. Boek nu uw verblijf in deze prachtige Bed and Breakfast in Vorden en ervaar zelf de unieke combinatie van gastvrijheid, comfort en de ongeëvenaarde ligging die De Schuilplaats te bieden heeft.`}
      image={require("../../../../assets/vooraanzicht.webp")}
    ></AppTextImage>
  );
}

export default TextArea;
