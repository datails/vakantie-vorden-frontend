import React from "react";
import makeStyles from "@mui/styles/makeStyles";
import createStyles from "@mui/styles/createStyles";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";
import ArrawBackIosIcon from "@mui/icons-material/ArrowBackIos";
import "react-responsive-carousel/lib/styles/carousel.min.css"; // requires a loader
import { Carousel } from "react-responsive-carousel";
import Image from "next/image";

const useStyles = makeStyles((theme) =>
  createStyles({
    fullWidth: {
      width: "100%",
    },
    images: {
      background: "#FFF",
    },
    widthAuto: {
      margin: "auto",
      width: "fit-content!important",
      maxHeight: "450px",
    },
    arrowRight: {
      cursor: "pointer",
      position: "absolute",
      top: "40%",
      bottom: "auto",
      padding: ".4em",
      zIndex: 2,
      right: "4.3em",
    },
    arrowLeft: {
      cursor: "pointer",
      position: "absolute",
      top: "40%",
      bottom: "auto",
      padding: ".4em",
      zIndex: 2,
      left: "4.3em",
    },
  })
);

function getRefToFile(file: { fileNames: string[] }) {
  const fileName = (file.fileNames || []).find((file) => file.includes("-m"));
  return `https://res.cloudinary.com/datails/image/upload/v1673074612/vakantie-vorden/${fileName}`;
}

type Props = {
  items: { fileNames: string[] }[];
};

export default function AppCarousel({ items }: Props) {
  const classes = useStyles();
  const images = items.map(getRefToFile);
  return (
    <Carousel
      // showThumbs={true}
      centerMode={true}
      infiniteLoop
      showStatus={false}
      renderArrowNext={(onClickHandler, hasNext) =>
        hasNext && (
          <div
            onClick={onClickHandler}
            className={classes.arrowRight}
            data-testid="next-btn"
          >
            <ArrowForwardIosIcon />
          </div>
        )
      }
      renderArrowPrev={(onClickHandler, hasPrev) =>
        hasPrev && (
          <div
            onClick={onClickHandler}
            className={classes.arrowLeft}
            data-testid="previous-btn"
          >
            <ArrawBackIosIcon />
          </div>
        )
      }
    >
      {images.map((image: string, id: number) => (
        <div key={id} className={classes.images}>
          <Image
            src={image}
            className={classes.widthAuto}
            alt={id?.toString()}
            width={1920}
            height={1080}
          />
        </div>
      ))}
    </Carousel>
  );
}
