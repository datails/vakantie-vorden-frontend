import React from "react";
import { render, screen } from "@testing-library/react";
import AppCarousel from "../index";
import "@testing-library/jest-dom";

describe("AppCarousel", () => {
  const mockItems = [
    { fileNames: ["image1-m.jpg"] },
    { fileNames: ["image2-m.jpg"] },
  ];

  it("renders AppCarousel with images", () => {
    render(<AppCarousel items={mockItems} />);
    const images = screen.getAllByRole("img");
    expect(images[0]).toBeInTheDocument();
    expect(images[1]).toBeInTheDocument();
  });

  it("renders AppCarousel with arrow buttons", () => {
    render(<AppCarousel items={mockItems} />);

    const nextButton = screen.getByTestId("next-btn");
    expect(nextButton).toBeInTheDocument();

    const prevButton = screen.getByTestId("previous-btn");
    expect(prevButton).toBeInTheDocument();
  });
});
