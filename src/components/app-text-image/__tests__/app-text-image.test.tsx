import React from "react";
import { render } from "@testing-library/react";
import AppTextImage from "../index";
import "@testing-library/jest-dom";

test("renders AppTextImage component", () => {
  const text = "This is a sample text";
  const title = "Sample Title";
  const image = "sample-image.jpg";
  const reverse = false;
  const backgroundColor = "#f0f0f0";
  const { getByText, getByAltText } = render(
    <AppTextImage
      text={text}
      title={title}
      image={image}
      reverse={reverse}
      backgroundColor={backgroundColor}
    />
  );
  const titleElement = getByText(title);
  expect(titleElement).toBeInTheDocument();
  const textElement = getByText(text);
  expect(textElement).toBeInTheDocument();
  const imageElement = getByAltText(title);
  expect(imageElement).toBeInTheDocument();
});
