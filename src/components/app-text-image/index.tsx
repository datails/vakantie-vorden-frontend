import React from "react";
import { Grid, Typography } from "@mui/material";
import ReactMarkdown from "react-markdown";
import useStyles from "./app-text-image.styles";
import Image from "next/image";

type Props = {
  text: string;
  title: string;
  reverse?: boolean;
  image?: string;
  iframe?: boolean;
  backgroundColor?: string;
  children?: any;
};

export default function AppTextImage({
  text,
  title,
  reverse = false,
  image,
  iframe,
  backgroundColor,
  children,
}: Props) {
  const classes = useStyles();

  return (
    <React.Fragment>
      <Grid
        container
        justifyContent="space-around"
        direction={reverse ? "row-reverse" : "row"}
        className={classes.container}
        spacing={10}
        style={{ backgroundColor: backgroundColor }}
      >
        <Grid item sm={12} lg={7} className={classes.content}>
          {title ? (
            <Typography variant="h2" gutterBottom className={classes.title}>
              {title}
            </Typography>
          ) : (
            ""
          )}
          <ReactMarkdown className={classes.text}>{text}</ReactMarkdown>
          {children}
        </Grid>
        {image && (
          <Grid item sm={12} lg={5} className={classes.content}>
            <Image
              src={image}
              alt={title}
              className={classes.image}
              layout="responsive"
            />
          </Grid>
        )}
        {iframe && (
          <Grid item sm={12} lg={5} className={classes.content}>
            <iframe
              width="100%"
              height="350"
              // frameborder="0"
              // scrolling="no"
              // marginheight="0"
              // marginwidth="0"
              src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=nl&amp;q=+(stichting%20(V)%C3%A9cht%20ervoor!)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"
            ></iframe>
          </Grid>
        )}
      </Grid>
    </React.Fragment>
  );
}
