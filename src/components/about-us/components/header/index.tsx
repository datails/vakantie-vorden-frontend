import React from "react";
import { AppHeroHeader } from "../../..";

function AppHeader() {
  return (
    <AppHeroHeader
      subTitle="Een vakantie die bij je past!"
      title="Over Vakantie in Vorden"
      background="vooraanzicht.webp"
    />
  );
}

export default AppHeader;
