import React from "react";
import { AppTextImage } from "../../..";

function TextArea() {
  return (
    <AppTextImage
      title="Over de verhuurders van Bed and Breakfast de Schuilplaats in Vorden"
      reverse
      text={`Als trotse inwoners van Vorden zetten wij ons in voor het aanbieden van kleinschalige vakantiewoningen en B&B's in de prachtige omgeving van Vorden, direct aan u, zonder tussenkomst van grote boekingsplatforms zoals Airbnb. Wij bieden een charmante Bed and Breakfast te huur aan, perfect gelegen nabij het serene landgoed het Enzerinck. Onze B&B is de ideale keuze voor diegenen die op zoek zijn naar rust en ontspanning, aan de rand van het betoverende bos.\n\nOnze Bed and Breakfast biedt u de unieke kans om te genieten van de rustige schoonheid van Vorden, weg van de drukte, en toch dichtbij genoeg om de lokale bezienswaardigheden gemakkelijk te verkennen. Of u nu op zoek bent naar een rustige retraite in de natuur of een actieve vakantie vol met wandelingen door het bos, onze B&B in Vorden is de perfecte uitvalsbasis voor uw avontuur.\n\nDoor te kiezen voor onze vakantiehuis of B&B in Vorden, steunt u lokale ondernemers en geniet u van een authentieke ervaring die met zorg en aandacht voor u is samengesteld. Ontdek de verborgen parels van Vorden, omarm de rust van de natuur en maak uw verblijf onvergetelijk met onze persoonlijke touch.\n\nPlan nu uw ontsnapping aan de dagelijkse sleur en boek uw verblijf in onze idyllisch gelegen Bed and Breakfast in Vorden. Ervaar de perfecte combinatie van comfort, gastvrijheid en de schoonheid van de natuur in Vorden, allemaal zonder de tussenkomst van grote partijen. Uw ideale vakantie in Vorden begint hier, in ons liefdevol aangeboden vakantiehuis en B&B.`}
      image={require("../../../../assets/eigenaren_de_schuilplaats_vorden.jpeg")}
    ></AppTextImage>
  );
}

export default TextArea;
