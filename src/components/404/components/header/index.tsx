import React from "react";
import AppHeader from "../../../app-hero-header";

export default function () {
  return (
    <AppHeader
      subTitle="Niet gevonden"
      title="404"
      background="de-schuilplaats-kachel.jpeg"
    />
  );
}
