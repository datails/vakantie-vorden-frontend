import React from "react";
import AppTextImage from "../../../app-text-image";

export default function TextArea() {
  return (
    <AppTextImage
      backgroundColor="#FFF"
      title="Pagina niet gevonden!"
      text={"Sorry deze pagina hebben wij niet kunnen vinden."}
    />
  );
}
