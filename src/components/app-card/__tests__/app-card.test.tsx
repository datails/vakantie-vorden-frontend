import React from "react";
import { render } from "@testing-library/react";
import AppCard from "../index";
import "@testing-library/jest-dom";

test("renders AppCard component", () => {
  const cardData = {
    buttonText: "Learn More",
    desc: "This is a description",
    image: "path/to/image.jpg",
    title: "Card Title",
    href: "/learn-more",
  };
  const { getByText } = render(<AppCard {...cardData} />);
  expect(getByText("Card Title")).toBeInTheDocument();
  expect(getByText("This is a description")).toBeInTheDocument();
  expect(getByText("Learn More")).toBeInTheDocument();
});
