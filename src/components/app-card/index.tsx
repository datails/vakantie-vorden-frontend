import React from "react";
import {
  Card,
  Grid,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
} from "@mui/material";
import Button from "@mui/material/Button";
//@ts-ignore
import classnames from "classnames";

import useStyles from "./app-card.styles";

type Props = {
  buttonText: string;
  desc: string;
  image: string;
  title: string;
  href: string;
};

export default function AppCard({
  buttonText,
  desc,
  image,
  title,
  href,
}: Props) {
  const classes = useStyles();

  return (
    <Card className={classes.card}>
      <Grid
        container
        justifyContent="space-around"
        className={classes.container}
        spacing={10}
      >
        <Grid item className={classes.content}>
          <CardMedia
            component="img"
            alt={title}
            height="140"
            image={image}
            title={title}
            className={classes.media}
          />
          <Grid
            container
            justifyContent="space-around"
            className={classes.container}
            spacing={10}
          >
            <Grid item xs={12} className={classes.content}>
              <CardContent
                className={classnames(classes.content, classes.padding)}
              >
                <Typography
                  gutterBottom
                  variant="h5"
                  component="h2"
                  className={classes.title}
                >
                  {title}
                </Typography>
                <Typography variant="body2" color="textSecondary" component="p">
                  {desc}
                </Typography>
              </CardContent>
            </Grid>
            <Grid item xs={12} className={classes.content}>
              <CardActions className={classes.cardActions}>
                <Typography
                  gutterBottom
                  variant="h5"
                  component="h2"
                ></Typography>
                <Button href={href} size="small" color="primary">
                  {buttonText}
                </Button>
              </CardActions>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Card>
  );
}
