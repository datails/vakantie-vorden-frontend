import "date-fns";
import React, { useContext, useEffect, useState } from "react";
import { Grid, Typography } from "@mui/material";
import dutchLocale from "date-fns/locale/nl";
import AppPersons from "../app-search/components/persons";
import { Context } from "../../store/store";
// @ts-ignore
import { MobileDatePicker } from "@mui/x-date-pickers";
// @ts-ignore
import { LocalizationProvider } from "@mui/x-date-pickers";
// @ts-ignore
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
// @ts-ignore
import classNames from "classnames";
import { validateDatesAndUpdateToValidDateRange } from "../../utils";
import { AppPriceList, AppBookButton } from "./components";
import { AppCirculairLoader } from "..";
import useStyles from "./app-booking.styles";

import type { ResponsiveStyleValue } from "@mui/system";
import type { GridDirection } from "@mui/material/Grid";

type Props = {
  direction?: ResponsiveStyleValue<GridDirection>;
  initialProperty: {
    id: number;
    persons: number;
    price: number;
    availability: {
      data: [
        {
          startDate: string | number | Date;
          endDate: string | number | Date;
          price: number;
        }
      ];
    };
    minNights: number;
    isVATIncluded: boolean;
    servicePrice: number;
  };
  readOnly?: boolean;
};

function AppBooking({
  direction = "row",
  initialProperty,
  readOnly = false,
}: Props) {
  const date: Date = new Date();
  const classes = useStyles();
  const [loading, setLoading] = useState<boolean>(true);
  const [state, dispatch] = useContext<any>(Context);

  const handleInvalidDates = () => {
    const { checkin, checkout } = validateDatesAndUpdateToValidDateRange({
      checkin: state.search.checkin,
      checkout: state.search.checkout,
      property: initialProperty,
    });

    dispatch({
      type: "SET_SEARCH",
      search: {
        checkin,
        checkout,
      },
    });

    setLoading(false);
  };

  const handleCheckInChange = (date: Date | undefined | null) => {
    dispatch({
      type: "SET_SEARCH",
      search: {
        checkin: date,
      },
    });
  };

  const handleCheckoutChange = (date: Date | undefined | null) => {
    dispatch({
      type: "SET_SEARCH",
      search: {
        checkout: date,
      },
    });
  };

  useEffect(() => {
    handleInvalidDates();
  }, []);

  if (loading) {
    return <AppCirculairLoader />;
  }

  return (
    <LocalizationProvider
      dateAdapter={AdapterDateFns}
      adapterLocale={dutchLocale}
    >
      <Grid
        container
        justifyContent="space-between"
        direction={direction}
        className={classes.container}
        spacing={10}
        component={"nav"}
      >
        <Grid className={classes.content}>
          <div>
            <MobileDatePicker
              minDate={date}
              label="Inchecken"
              format="MM/dd/yyyy"
              value={state.search.checkin}
              onChange={handleCheckInChange}
              className={classes.datePicker}
              disabled={readOnly}
              slotProps={{ textField: { variant: "standard" } }}
            />
          </div>
          {state.search.checkin && (
            <MobileDatePicker
              minDate={state.search.checkin}
              label="Uitchecken"
              format="MM/dd/yyyy"
              value={state.search.checkout}
              onChange={handleCheckoutChange}
              className={classes.datePicker}
              disabled={readOnly}
              slotProps={{ textField: { variant: "standard" } }}
            />
          )}
        </Grid>
        <Grid className={classNames(classes.content, classes.justifyCenter)}>
          <div className={classes.datePicker}>
            <AppPersons />
          </div>
        </Grid>
        <>
          {!readOnly && (
            <Grid
              className={classNames(classes.content, classes.justifyCenter)}
            >
              <AppBookButton property={initialProperty} />
            </Grid>
          )}
          <Grid className={classes.content}>
            <Typography
              component="p"
              variant="body1"
              gutterBottom
              className={classes.subTitle}
            >
              <AppPriceList
                property={initialProperty}
                checkin={state.search.checkin}
                checkout={state.search.checkout}
              />
            </Typography>
          </Grid>
        </>
      </Grid>
    </LocalizationProvider>
  );
}

export default AppBooking;
