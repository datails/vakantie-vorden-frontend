import React, { useContext } from "react";
import { Divider } from "@mui/material";
import { isBefore } from "date-fns";
import {
  getTotalPriceOfProperty,
  getDifferenceInDays,
  parseDateOrString,
} from "../../../../utils";

import { Context } from "../../../../store/store";

import useStyles from "./price-list.styles";

const getTotalTaxPrice = (
  state: any,
  property: { minNights: number },
  checkin: Date,
  checkout: Date
) => {
  const guests =
    state.search.adults + state.search.children + state.search.babies;
  const taxPerNight = 1.3;
  const lengthOfStay = getDifferenceInDays(
    new Date(parseDateOrString(checkin) || new Date()).toISOString(),
    new Date(parseDateOrString(checkout) || new Date()).toISOString(),
    property.minNights
  );

  return lengthOfStay * taxPerNight * guests;
};

const getTotalPrice = (
  property: {
    price: number;
    availability: {
      data: [
        {
          startDate: string | number | Date;
          endDate: string | number | Date;
          price: number;
        }
      ];
    };
    minNights: number;
  },
  checkin: Date,
  checkout: Date
) => {
  const checkIn = new Date(parseDateOrString(checkin) || new Date());
  const checkOut = new Date(parseDateOrString(checkout) || new Date());

  return isBefore(checkIn, checkOut)
    ? getTotalPriceOfProperty(
        property.price,
        checkIn.toISOString(),
        checkOut.toISOString(),
        property.availability.data,
        property.minNights
      )
    : 0;
};

type Props = {
  property: {
    price: number;
    availability: {
      data: [
        {
          startDate: string | number | Date;
          endDate: string | number | Date;
          price: number;
        }
      ];
    };
    minNights: number;
    isVATIncluded: boolean;
    servicePrice: number;
  };
  checkin: Date;
  checkout: Date;
};

function AppPriceList({ property, checkin, checkout }: Props) {
  const classes = useStyles();
  const [state] = useContext<any>(Context);

  const totalTaxPrice = getTotalTaxPrice(state, property, checkin, checkout);
  const totalPrice = getTotalPrice(property, checkin, checkout);

  return (
    <React.Fragment>
      <div className={classes.content}>
        <span>
          Prijs{" "}
          {property.isVATIncluded ? (
            <i>(Inclusief BTW 21%)</i>
          ) : (
            <i>(Exc. BTW)</i>
          )}
        </span>
        <span>
          {new Intl.NumberFormat("nl-NL", {
            style: "currency",
            currency: "EUR",
          }).format(totalPrice)}
        </span>
      </div>
      <div className={classes.content}>
        <span>Servicekosten</span>
        <span>
          {new Intl.NumberFormat("nl-NL", {
            style: "currency",
            currency: "EUR",
          }).format(property.servicePrice)}
        </span>
      </div>
      <div className={classes.content}>
        <span>Toeristenbelasting</span>
        <span>
          {new Intl.NumberFormat("nl-NL", {
            style: "currency",
            currency: "EUR",
          }).format(totalTaxPrice)}
        </span>
      </div>
      <Divider className={classes.marginTop} />
      <div className={classes.content}>
        <b>Totaal</b>
        <b>
          {new Intl.NumberFormat("nl-NL", {
            style: "currency",
            currency: "EUR",
          }).format(totalPrice + property.servicePrice + totalTaxPrice)}
        </b>
      </div>
    </React.Fragment>
  );
}

export default AppPriceList;
