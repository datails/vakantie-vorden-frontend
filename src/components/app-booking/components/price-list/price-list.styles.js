import makeStyles from '@mui/styles/makeStyles';

export default makeStyles(() => ({
  content: {
    display: "flex",
    justifyContent: "space-between",
    marginTop: 30,
    width: "100%",
  },
  marginTop: {
    marginTop: 20,
  },
}));
