import React, { useContext } from "react";
import { useRouter } from "next/navigation";

import { AppButton } from "../../..";
import { Context } from "../../../../store/store";

import useStyles from "./reservation-button.styles";

const isCheckinAndCheckoutFilled = (state: {
  search: { checkin: Date; checkout: Date };
}) => state.search.checkout && state.search.checkin;
const hasAdults = (state: { search: { adults: number } }) =>
  state.search.adults > 0;

const isMaximumGuestsReached = (
  state: { search: { children: number; babies: number; adults: number } },
  property: { persons: number }
) =>
  state.search.adults + state.search.children + state.search.babies >
  property.persons;

const isButtonEnabled = (
  state: {
    search: {
      checkout: Date;
      checkin: Date;
      adults: number;
      children: number;
      babies: number;
    };
  },
  property: {
    persons: number;
  }
) =>
  hasAdults(state) &&
  isCheckinAndCheckoutFilled(state) &&
  !isMaximumGuestsReached(state, property);

type Props = {
  property: {
    id: number;
    persons: number;
  };
};

function AppBookButton({ property }: Props) {
  const [state, dispatch] = useContext<any>(Context);
  const classes = useStyles();
  const router = useRouter();

  const handleReservation = () => {
    const parsedProperty = JSON.parse(JSON.stringify(property));

    dispatch({
      type: "SET_PROPERTY",
      property: {
        ...parsedProperty,
      },
    });

    router.push(
      `/vakantiehuisjes/${property.id}/reserveren`
      // , false
    );
  };

  return (
    <AppButton
      disabled={!isButtonEnabled(state, property)}
      handler={() => handleReservation()}
      styles={{
        background: classes.button,
      }}
    >
      Reserveren
    </AppButton>
  );
}

export default AppBookButton;
