import createStyles from '@mui/styles/createStyles';
import makeStyles from '@mui/styles/makeStyles';

export default makeStyles((theme) =>
  createStyles({
    button: {
      backgroundColor: theme.palette.primary.main,
    },
  })
);
