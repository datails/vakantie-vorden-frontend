import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';

export default makeStyles((theme) =>
  createStyles({
    backgroundGreen: {
      background: "transparent",
      height: 64,
      width: "100%",
      display: "flex",
      marginTop: "-64px",
    },
  })
);
