import React, {
  useContext,
  useState,
  createRef,
  RefObject,
  FormEvent,
} from "react";
//@ts-ignore
import ReCAPTCHA from "react-google-recaptcha";
import { FormControl, TextField, Button, Typography } from "@mui/material";
import { useRouter } from "next/navigation";

import * as U from "./form.utils";
import useStyles from "./form.styles";
import { Context } from "../../../../store/store";
import { AppSnackBar } from "../../../../framework";
import { createClientAndReservation } from "../../../../utils";

type Props = {
  children: React.ReactNode;
};

const Message = ({ children }: Props) => {
  return (
    <Typography gutterBottom variant="h5" component="h2">
      {children}
    </Typography>
  );
};

function AppForm() {
  const classes = useStyles();
  const recaptchaRef: RefObject<any> = createRef();
  const [reservation, setReservation] = useState({});
  const [, setValue] = useState("Controlled");
  const [, setLoading] = useState(false);
  const [state, dispatch] = useContext<any>(Context);
  const [snackBar, setSnackBar] = useState({
    open: false,
    message: "",
    severity: "",
  });
  const router = useRouter();

  const handleChange = (event: { target: { value: string } }) => {
    setValue(event.target.value);
  };

  const handleClose = (_event: any, reason: string) => {
    if (reason === "clickaway") {
      return;
    }

    setSnackBar({
      open: false,
      message: "",
      severity: "",
    });
  };

  const formHandler = async (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const recaptchaValue = recaptchaRef.current.getValue();
    const { isError, error } = U.validate(state);
    const client = U.parseForm(event);

    if (isError) {
      return setSnackBar({
        open: true,
        message: error,
        severity: "error",
      });
    }

    dispatch({
      type: "SET_CLIENT",
      client,
    });

    const resp = await createClientAndReservation({
      client,
      reservation: U.reservation(state),
      recaptchaValue,
    }).finally(() => {
      setLoading(false);
    });

    return resp.error
      ? setSnackBar({
          open: true,
          message: resp.errorMessage,
          severity: "error",
        })
      : setReservation(resp.reservation);
  };

  if (Object.keys(reservation).length) {
    router.push(
      `/vakantiehuisjes/${state.property.id}/boeking/`
    );
  }

  return (
    <form id="contact-form" onSubmit={formHandler} className={classes.form}>
      <FormControl variant="standard" className={classes.form}>
        <TextField
          id="firstname"
          label="Voornaam"
          name="firstname"
          variant="outlined"
          onChange={handleChange}
          className={classes.textfield}
          defaultValue={state.client?.firstname}
          required
        />
        <TextField
          id="insertion"
          label="Tussenvoegsel"
          name="insertion"
          variant="outlined"
          onChange={handleChange}
          className={classes.textfield}
          defaultValue={state.client?.insertion}
        />
        <TextField
          id="lastname"
          label="Achternaam"
          name="lastname"
          variant="outlined"
          onChange={handleChange}
          className={classes.textfield}
          defaultValue={state.client?.lastname}
          required
        />
        <TextField
          id="street"
          label="Straat"
          name="street"
          variant="outlined"
          onChange={handleChange}
          className={classes.textfield}
          defaultValue={state.client?.street}
          required
        />
        <TextField
          type="number"
          id="housenumber"
          label="Huisnummer"
          name="housenumber"
          variant="outlined"
          onChange={handleChange}
          className={classes.textfield}
          defaultValue={state.client?.housenumber}
          required
        />
        <TextField
          id="housenumberextension"
          label="Huisnummerextensie"
          name="housenumberextension"
          variant="outlined"
          onChange={handleChange}
          className={classes.textfield}
          defaultValue={state.client?.housenumberextension}
        />
        <TextField
          id="city"
          label="Stad"
          name="city"
          variant="outlined"
          onChange={handleChange}
          className={classes.textfield}
          defaultValue={state.client?.city}
          required
        />
        <TextField
          id="zipcode"
          label="Postcode"
          name="zipcode"
          variant="outlined"
          onChange={handleChange}
          className={classes.textfield}
          defaultValue={state.client?.zipcode}
          required
        />
        <TextField
          type="email"
          id="email"
          label="Email"
          name="email"
          variant="outlined"
          onChange={handleChange}
          className={classes.textfield}
          defaultValue={state.client?.email}
          required
        />
        <TextField
          type="tel"
          id="telephone"
          label="Telefoonnummer"
          name="telephone"
          variant="outlined"
          onChange={handleChange}
          className={classes.textfield}
          defaultValue={state.client?.telephone}
        />
        <ReCAPTCHA
          ref={recaptchaRef}
          sitekey="6LcZVo0aAAAAAAuVxJxGazVmV7eer7btPk25SOk5"
        />
        <Button className={classes.button} type="submit">
          Verzenden
        </Button>
        {snackBar.open && (
          <AppSnackBar
            open={snackBar.open}
            message={snackBar.message}
            // severity={snackBar.severity}
            handler={handleClose}
          />
        )}
      </FormControl>
    </form>
  );
}

export default AppForm;
