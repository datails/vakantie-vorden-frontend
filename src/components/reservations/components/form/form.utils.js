import * as R from 'ramda';
import { content } from './form.content';

export const toObject = (acc, [ key, value ]) => ({
    ...acc,
    [key]: value
})

export const parseForm = (event) => {
    const formData = [...new FormData(event.target)];
    return R.reduce(toObject, {}, formData)
}

export const reservation = (state) => {
    return {
        startDate: state.search.checkin,
        endDate: state.search.checkout,
        status: "aanvraag",
        isPaid: false,
        adults: state.search.adults,
        children: state.search.children,
        property: state.property,
    }
}

export const validate = (state) => {
    let error = ''
  
    if (!state.search.checkin || !state.search.checkout) {
      error = content.validate.invalidDate
    }
  
    if (!state.property) {
      error = content.validate.invalidProperty
    }
  
    return {
      isError: Boolean(error),
      error,
    }
  }