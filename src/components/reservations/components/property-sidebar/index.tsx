import React, { useContext } from "react";
import { Paper, Typography } from "@mui/material";

import { content } from "./property.content";
import useStyles from "./property-sidebar.styles";
import { Context } from "../../../../store/store";
import AppBooking from "../../../app-booking";

type Props = {
  property: {
    id: number;
    persons: number;
    price: number;
    availability: {
      data: [
        {
          startDate: string | number | Date;
          endDate: string | number | Date;
          price: number;
        }
      ];
    };
    minNights: number;
    isVATIncluded: boolean;
    servicePrice: number;
  };
};

function AppAvailabilitySidebar({ property }: Props) {
  const classes = useStyles();
  const [state] = useContext<any>(Context);
  const isValidBooking =
    state.property && state.search.checkin && state.search.checkout;

  if (!isValidBooking || !property) return null;

  return (
    <Paper
      component="aside"
      className={classes.sideSearch}
      id="search-container"
    >
      <AppBooking initialProperty={property} readOnly />
      <br />
      <Typography
        gutterBottom
        // variant="p"
        component="p"
      >
        {content.details}
      </Typography>
    </Paper>
  );
}

export default AppAvailabilitySidebar;
