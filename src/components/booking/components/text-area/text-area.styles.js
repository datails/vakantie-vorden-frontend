import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';

export default makeStyles((theme) =>
  createStyles({
    margin: {
      marginBottom: 30,
    },
  })
);
