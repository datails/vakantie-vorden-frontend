import React from "react";
import { render, fireEvent, screen } from "@testing-library/react";
import CustomizedDialogs from "../index";
import "@testing-library/jest-dom";

test("renders CustomizedDialogs component", () => {
  const title = "Test Dialog";
  const html = "<p>This is a test dialog content.</p>";
  const handleClose = jest.fn();
  const open = true;
  const { getByText } = render(
    <CustomizedDialogs
      title={title}
      html={html}
      handleClose={handleClose}
      open={open}
    />
  );
  expect(getByText(title)).toBeInTheDocument();
  expect(getByText("This is a test dialog content.")).toBeInTheDocument();
  fireEvent.click(screen.getByLabelText("close"));
  expect(handleClose).toHaveBeenCalledTimes(1);
});
