import React, { MouseEventHandler } from "react";
import withStyles from "@mui/styles/withStyles";
import makeStyles from "@mui/styles/makeStyles";
import createStyles from "@mui/styles/createStyles";
import Dialog from "@mui/material/Dialog";
import MuiDialogTitle from "@mui/material/DialogTitle";
import MuiDialogContent from "@mui/material/DialogContent";
import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";
import Typography from "@mui/material/Typography";
import { DefaultTheme } from "@mui/styles";
import { ClassNameMap } from "@mui/material";

const styles = (theme: Record<string, any>) =>
  createStyles({
    root: {
      margin: 0,
      padding: theme.spacing(2),
    },
    closeButton: {
      position: "absolute",
      right: theme.spacing(1),
      top: theme.spacing(1),
      color: theme.palette.grey[500],
    },
  });

const useStyles = makeStyles((theme: DefaultTheme) =>
  createStyles({
    content: {
      fontSize: 18,
    },
  })
);

type DialogTitleProps = {
  children: React.ReactNode;
  classes: ClassNameMap;
  onClose: React.MouseEventHandler<HTMLButtonElement>;
  id: string;
};

const DialogTitle = withStyles(styles)((props: DialogTitleProps) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle
      //  disableTypography
      className={classes.root}
      {...other}
    >
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton
          aria-label="close"
          className={classes.closeButton}
          onClick={onClose}
          size="large"
        >
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiDialogContent);

type Props = {
  title: string;
  html: string | TrustedHTML;
  handleClose: MouseEventHandler<HTMLButtonElement>;
  open: boolean;
  children?: any;
};

export default function CustomizedDialogs({
  title,
  html,
  handleClose,
  open,
  children,
}: Props) {
  const classes = useStyles();

  return (
    <div>
      <Dialog
        onClose={handleClose}
        aria-labelledby="customized-dialog-title"
        open={open}
      >
        <DialogTitle id="customized-dialog-title" onClose={handleClose}>
          {title}
        </DialogTitle>
        <DialogContent dividers>
          {html ? (
            <div
              className={classes.content}
              dangerouslySetInnerHTML={{ __html: html }}
            ></div>
          ) : (
            children
          )}
        </DialogContent>
      </Dialog>
    </div>
  );
}
