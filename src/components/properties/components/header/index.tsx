import React from "react";
import { AppHeroHeader } from "../../..";

function AppHeader() {
  return (
    <AppHeroHeader
      subTitle="Een vakantie die bij je past!"
      title="Vakantiehuisjes in vorden"
      background="de-schuilplaats-zomer.jpeg"
    />
  );
}

export default AppHeader;
