import React from "react";
import { Paper, Typography } from "@mui/material";

import useAvailabilitySideBarStyles from "./availability-sidebar.styles";
import AppBooking from "../../../app-booking";

type Props = {
  property: {
    id: number;
    persons: number;
    price: number;
    availability: {
      data: [
        {
          startDate: string | number | Date;
          endDate: string | number | Date;
          price: number;
        }
      ];
    };
    minNights: number;
    isVATIncluded: boolean;
    servicePrice: number;
  };
};

function AppAvailabilitySidebar({ property }: Props) {
  const classes = useAvailabilitySideBarStyles();

  return (
    <Paper
      component="aside"
      className={classes.sideSearch}
      id="search-container"
    >
      <Typography
        gutterBottom
        variant="h5"
        component="h2"
        // className={classes.sideBarTitle}
      >
        Direct boeken
      </Typography>
      <AppBooking initialProperty={property} />
    </Paper>
  );
}

export default AppAvailabilitySidebar;
