import React from "react";
import { Chip, Typography } from "@mui/material";
import DoneIcon from "@mui/icons-material/Done";
import useStyles from "./provisions.styles";
import { translate } from "../../../../../../framework/app-catalogue/app-catalogue.content";

type Props = {
  property: any;
};

function AppProvisions({ property }: Props) {
  const classes = useStyles();
  const handler = () => {};

  return (
    <>
      <Typography
        component="h2"
        variant="h4"
        gutterBottom
        className={classes.title}
      >
        Voorzieningen
      </Typography>
      <div className={classes.root}>
        {Object.keys(property).map((key) => {
          if (key.startsWith("has") && property[key] === true) {
            return (
              <Chip
                color="secondary"
                // @ts-ignore
                label={translate[key]}
                deleteIcon={<DoneIcon />}
                onClick={handler}
                onDelete={handler}
              />
            );
          }
          return null;
        })}
      </div>
    </>
  );
}

export default AppProvisions;
