import React, { useContext } from "react";
import { List, ListItem, ListItemText, ListItemIcon } from "@mui/material";
import { MeetingRoom, EventBusy, MenuBook, Wifi } from "@mui/icons-material";

import { addDays, format } from "date-fns";
import dutchLocale from "date-fns/locale/nl";

import AppRulesModal from "../rules-modal";

import useStyles from "./details.styles";
import { Context } from "../../../../../../store/store";
import { parseDateOrString } from "../../../../../../utils";
import type { Moment } from "moment";

type Props = {
  property: {
    numberOfDaysBeforeCancel: number;
    rules: string | TrustedHTML;
    hasWifi: boolean;
    checkin: Moment;
    isFitForAnimals: boolean;
    isSmokingProperty: boolean;
  };
};

function AppDetails({ property }: Props) {
  const classes = useStyles();
  const [state] = useContext<any>(Context);
  const [open, setOpen] = React.useState(false);



  const checkin: Date = new Date(
    parseDateOrString(state?.search?.checkin || new Date())
  );

  const day = addDays(checkin, property.numberOfDaysBeforeCancel);

  const formattedDay = format(day, "d MMMM", {
    locale: dutchLocale,
  });

  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <>
      <AppRulesModal
        rules={property.rules}
        // handleOpen={handleOpen}
        handleClose={handleClose}
        open={open}
        // setOpen={setOpen}
      />
      <List>
        {property.hasWifi && (
          <ListItem className={classes.listItem}>
            <ListItemIcon>
              <Wifi />
            </ListItemIcon>
            <ListItemText primary={`Gratis WiFi`} />
          </ListItem>
        )}
        <ListItem className={classes.listItem}>
          <ListItemIcon>
            <MeetingRoom />
          </ListItemIcon>
          <ListItemText primary={`Inchecken om ${property.checkin}.`} />
        </ListItem>
        <ListItem className={classes.listItem}>
          <ListItemIcon>
            <EventBusy />
          </ListItemIcon>
          <ListItemText
            primary={`Gratis annuleren tot ${formattedDay}`}
            secondary={`Annuleer gratis tot ${formattedDay}, met uitzondering van reserveringskosten.`}
          />
        </ListItem>
        <ListItem className={classes.listItem} button onClick={handleOpen}>
          <ListItemIcon>
            <MenuBook />
          </ListItemIcon>
          <ListItemText
            primary={"Huisregels"}
            secondary={`${!property.isSmokingProperty ? "Roken" : ""} en ${
              !property.isFitForAnimals ? "huisdieren" : ""
            } zijn niet toegestaan. Bekijk alle huisregels.`}
          />
        </ListItem>
      </List>
    </>
  );
}

export default AppDetails;
