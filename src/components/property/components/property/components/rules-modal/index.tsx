import React from "react";
import Modal from "../../../../../app-modal";

type Props = {
  rules: string | TrustedHTML;
  // handleOpen: any;
  handleClose: () => void;
  open: boolean;
  // setOpen: any;
};

export default function ({
  rules,
  // handleOpen,
  handleClose,
  open,
}: // setOpen,
Props) {
  return (
    <Modal
      title={"Huisregels"}
      html={rules}
      // handleOpen={handleOpen}
      handleClose={handleClose}
      open={open}
      // setOpen={setOpen}
    />
  );
}
