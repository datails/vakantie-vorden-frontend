import makeStyles from '@mui/styles/makeStyles';

export default makeStyles(() => ({
  dayContentWrapper: {
    position: "relative",
  },
  dayContentPrice: {
    position: "absolute",
    top: 0,
    left: 10,
    marginTop: "-30%",
    fontSize: "10px",
  },
}));
