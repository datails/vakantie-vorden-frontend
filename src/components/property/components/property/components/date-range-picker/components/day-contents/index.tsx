import React from "react";
import type { Moment } from "moment";
import * as moment from "moment";
import { parseISO } from "date-fns";
import useStyles from "./day-contents.styles";
import * as utils from "../../utils";
import { extendMoment } from "moment-range";

type Props = {
  day: Moment;
  property: { price: number };
  availability: [
    {
      isAvailable: boolean;
      startDate: string | number | Date;
      endDate: string | number | Date;
      price: number;
    }
  ];
};

function DayContents({ day, property, availability }: Props) {
  const classes = useStyles();
  const rangeMoment = extendMoment(moment);
  const formattedDay = parseISO(rangeMoment(day).toISOString());

  return (
    <div className={classes.dayContentWrapper}>
      <span className={classes.dayContentPrice}>
        {new Intl.NumberFormat("nl-NL", {
          style: "currency",
          currency: "EUR",
        }).format(
          utils.getCorrectPrice(property.price, formattedDay, availability)
        )}
      </span>
      <span>{day.format("D")}</span>
    </div>
  );
}

export default DayContents;
