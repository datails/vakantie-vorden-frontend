import "moment/locale/nl";
import "react-dates/initialize";
import "react-dates/lib/css/_datepicker.css";
import React, { useState, useContext } from "react";
import { Typography } from "@mui/material";
import * as moment from "moment";
import { extendMoment } from "moment-range";

//@ts-ignore
import { DayPickerRangeController } from "react-dates";

import useStyles from "./date-range-picker.styles";
import { DayContents } from "./components";
import * as utils from "./utils";
import { Context } from "../../../../../../store/store";
import type { Moment } from "moment";

const rangeMoment = extendMoment(moment);

type Props = {
  property: {
    price: number;
    minNights: number;
    numberOfDaysBeforeCancel: number;
    availability: {
      blockedDays: [Moment];
      data: any;
    };
    blockedDays: [Moment];
  };
};

function AppDateRangePicker({ property }: Props) {
  const classes = useStyles();
  const [state, dispatch] = useContext<any>(Context);
  const [focusedInput, setFocusedInput] = useState<string>("startDate");

  const priceSlots = property.availability?.data.filter?.(
    (prices: any) => prices.isAvailable
  );

  return (
    <>
      <Typography
        component="h2"
        variant="h4"
        gutterBottom
        className={classes.title}
      >
        Beschikbaarheid
      </Typography>
      <DayPickerRangeController
        isOutsideRange={utils.disableToday}
        navPosition="navPositionBottom"
        keepOpenOnDateSelect={true}
        minDate={rangeMoment()}
        minimumNights={property.minNights}
        startDate={utils.startDate(state)}
        endDate={utils.endDate(state, property)}
        onDatesChange={utils.setStateOnChange(dispatch, state, property)}
        focusedInput={focusedInput}
        onFocusChange={(fcsInput: string) => {
          setFocusedInput(fcsInput);
        }}
        renderDayContents={(day: any, modifiers: any) => {
          day._locale._weekdaysMin = ["ZO", "MA", "DI", "WO", "DO", "VR", "ZA"];
          return (
            <DayContents
              availability={priceSlots}
              property={property}
              day={day}
            />
          );
        }}
        numberOfMonths={1}
        isDayBlocked={(day: Moment) => {
          return utils.isDayBlocked({
            day,
            blockedDays: property.blockedDays,
            minNights: property.minNights,
          });
        }}
        orientation="horizontal"
        daySize={100}
        calendarInfoPosition="after"
        hideKeyboardShortcutsPanel
        noBorder
      />
    </>
  );
}

export default AppDateRangePicker;
