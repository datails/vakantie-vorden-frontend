import makeStyles from '@mui/styles/makeStyles';

export default makeStyles(() => ({
  title: {
    marginTop: 30,
    marginBottom: 30,
    textAlign: "left",
  },
}));
