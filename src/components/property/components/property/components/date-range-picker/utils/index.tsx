import "moment/locale/nl";
import { addDays, parseISO, isWithinInterval } from "date-fns";
import * as moment from "moment";
import { extendMoment } from "moment-range";
import type { Moment } from "moment";
import { start } from "repl";

const rangeMoment = extendMoment(moment);

type Props = {
  day: Moment;
  blockedDays: any;
  minNights: number;
};

type StringOrNumberOrDate = string | number | Date;

export function isDayBlocked({ day, blockedDays, minNights }: Props) {
  return blockedDays.some((d: Moment) => d.isSame(day, "day"));
}

export const disableToday = (day: Moment) => day.isSame(rangeMoment(), "day");

export const startDate = (state: { search: { checkin: Moment } }) =>
  rangeMoment(state?.search?.checkin || new Date());
export const endDate = (
  state: { search: { checkout: Moment; checkin: Moment } },
  property: { numberOfDaysBeforeCancel: number }
) =>
  rangeMoment(
    state?.search?.checkout ||
      addDays(
        parseISO((state?.search?.checkin)?.toString() || new Date()?.toString()),
        property.numberOfDaysBeforeCancel
      )
  );

type StartEndDateProps = {
  startDate: StringOrNumberOrDate;
  endDate: StringOrNumberOrDate;
};

export const setStateOnChange =
  (
    dispatch: (arg: any) => void,
    state: {
      startDate: StringOrNumberOrDate;
      endDate: StringOrNumberOrDate;
      search: any;
    },
    property: { availability: { blockedDays: [Moment] } }
  ) =>
  ({ startDate, endDate }: StartEndDateProps) => {
    if (
      !isDateBlocked(startDate, endDate, property.availability?.blockedDays)
    ) {
      dispatch({
        type: "SET_SEARCH",
        search: {
          ...state.search,
          checkin: new Date(startDate),
          checkout: new Date(endDate),
        },
      });
    } else {
      dispatch({
        type: "SET_SEARCH",
        search: {
          ...state.search,
          checkin: new Date(startDate),
        },
      });
    }
  };

export function isDateBlocked(
  start: StringOrNumberOrDate,
  end: StringOrNumberOrDate,
  data: [Moment]
) {
  const dateFormat = "YYYY-MM-DD";
  const diff = rangeMoment(end).diff(start, "days") + 1;

  for (let i = 0; i < diff; i++) {
    const checkDate = rangeMoment(start).add(i, "d").format(dateFormat);

    const item = data.find((d) => {
      return rangeMoment(d).format("YYYY-MM-DD") === checkDate;
    });

    if (item) {
      return true;
    }
  }

  return false;
}

export function isDayBetweenDate(
  day: Date,
  availability: [
    { startDate: StringOrNumberOrDate; endDate: StringOrNumberOrDate }
  ]
) {
  return availability?.some?.(({ startDate, endDate }: StartEndDateProps) =>
    isWithinInterval(day, {
      start: parseISO(startDate?.toString()),
      end: parseISO(endDate?.toString()),
    })
  );
}

export function getTimeSlot(
  day: Date,
  availability: [
    {
      startDate: StringOrNumberOrDate;
      endDate: StringOrNumberOrDate;
      price: number;
    }
  ]
) {
  return availability.find(({ startDate, endDate }: StartEndDateProps) =>
    isWithinInterval(day, {
      start: parseISO(startDate?.toString()),
      end: parseISO(endDate?.toString()),
    })
  );
}

export function getCorrectPrice(
  price: number,
  day: Date,
  availability: [
    {
      startDate: StringOrNumberOrDate;
      endDate: StringOrNumberOrDate;
      price: number;
    }
  ]
) {
  const isDateBetweenDates = isDayBetweenDate(day, availability);
  if (!isDateBetweenDates) {
    return price;
  }

  const timeSlot = getTimeSlot(day, availability);
  if (timeSlot) return timeSlot.price;
  throw Error("Error, no time slot found");
}
