import React from "react";
import { Grid, Typography, Divider } from "@mui/material";
import Head from "next/head";
import AppSlider from "../../../app-carousel";
import { AppDateRangePicker, AppDetails, AppProvisions } from "./components";

import usePropertyStles from "./property.styles";

import type { Moment } from "moment";

type Props = {
  property: {
    title: string;
    subtitle: string;
    rules: string;
    hasWifi: boolean;
    checkin: Moment;
    isFitForAnimals: boolean;
    isSmokingProperty: boolean;
    url: string;
    id: number;
    files: {
      fileNames: string[];
    }[];
    persons: number;
    beds: number;
    minNights: number;
    squareMeters: number;
    description: string;
    price: number;
    numberOfDaysBeforeCancel: number;
    availability: {
      blockedDays: [Moment];
      data: [{ isAvailable: boolean }];
    };
    blockedDays: [Moment];
  };
};

const Property = ({ property }: Props) => {
  const classes = usePropertyStles();

  return (
    <article>
      <Head>
        <title>{property.title}</title>
        <meta name="description" content={property.subtitle} />
        <meta property="og:image" content={property.url} />
        <meta property="og:description" content={`${property.title}`} />
        <meta property="og:title" content={property.title} />
        <meta
          property="og:url"
          content={`https://vakantie-vorden.nl/vakantiehuisjes/${property.id}`}
        />
        <meta property="og:type" content="article" />
        <meta property="og:site_name" content="vakantie-vorden.nl" />
      </Head>
      <Grid
        container
        className={classes.container}
        justifyContent="space-evenly"
      >
        <Grid item xs={12} justifyContent="center" textAlign="center">
          <AppSlider items={property.files} />
          <Typography
            component="h1"
            variant="h4"
            gutterBottom
            className={classes.title}
          >
            {property.title}
            {/* <Avatar
              alt={property.owner.name}
              src={property.owner.file.originalname}
              className={classes.avatar}
            /> */}
          </Typography>
          <Typography
            component="h2"
            variant="body1"
            gutterBottom
            className={classes.subTitle}
          >
            {property.persons} gasten | {property.beds} bedden | minimaal{" "}
            {property.minNights} nachten | {property.squareMeters} vierkante
            meter
          </Typography>
          <Divider className={classes.divider} />
          <AppDetails property={property} />
          <Divider className={classes.divider} />
          <section
            className={classes.desc}
            dangerouslySetInnerHTML={{ __html: property.description }}
          ></section>
          <Divider className={classes.divider} />
          <AppProvisions property={property} />
          <Divider className={classes.divider} />
          <AppDateRangePicker property={property} />
        </Grid>
      </Grid>
    </article>
  );
};

export default Property;
