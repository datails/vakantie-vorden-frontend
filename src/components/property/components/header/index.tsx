import React from "react";
import useHeaderStyles from "./header.styles";

function AppPropertyHeader() {
  const classes = useHeaderStyles();
  return <div className={classes.backgroundGreen}></div>;
}

export default AppPropertyHeader;
