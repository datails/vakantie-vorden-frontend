import React from "react";
import { render } from "@testing-library/react";
import AppCard from "../index";
import "@testing-library/jest-dom";

const img = "de-schuilplaats-douche.jpeg";

describe("AppCard", () => {
  const props = {
    title: "Sample Title",
    desc: "Sample Description",
    background: img,
  };

  it("renders with correct title and description", () => {
    const { getByText } = render(<AppCard {...props}>C</AppCard>);
    expect(getByText("Sample Title")).toBeInTheDocument();
    expect(getByText("Sample Description")).toBeInTheDocument();
  });

  it("renders children inside Avatar", () => {
    const { getByText } = render(<AppCard {...props}>C</AppCard>);
    expect(getByText("C")).toBeInTheDocument();
  });
});
