import React, { useContext } from "react";
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  TextField,
} from "@mui/material";
import withStyles from "@mui/styles/withStyles";
import PersonSelector from "../person-select";
import { Context } from "../../../../store/store";
import useStyles from "./persons.styles";

const CustomTextField = withStyles({
  root: {
    "& label": {
      textTransform: "none",
    },
    "& .MuiInput-underline": {
      "&:after": {
        borderBottom: "initial",
      },
    },
  },
})(TextField);

function AppPersons() {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);
  const [state] = useContext<any>(Context);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <React.Fragment>
      <div className={classes.fullWidth}>
        {typeof state.search.adults === "number" && (
          <CustomTextField
            label="Reisgezelschap"
            value={`${state.search.adults + state.search.children} gasten`}
            onClick={handleClickOpen}
            variant="standard"
          />
        )}
        <Dialog open={open} onClose={handleClose}>
          <DialogTitle>Reisgezelschap</DialogTitle>
          <DialogContent className={classes.dialog}>
            <PersonSelector />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClose} color="primary">
              Annuleren
            </Button>
            <Button onClick={handleClose} color="primary">
              Ok
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    </React.Fragment>
  );
}

export default AppPersons;
