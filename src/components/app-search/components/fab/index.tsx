import React from "react";
import { Fab } from "@mui/material";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import useStyles from "./fab.styles";

type Props = {
  name: string;
  desc: string;
  state: number;
  setState: (number: number) => void;
};

function AppFab({ name, desc, state = 0, setState }: Props) {
  const classes = useStyles();

  const handleMinus = () => {
    if (state === 0) {
      return;
    }

    setState(state - 1);
  };

  const handlePlus = () => {
    setState(state + 1);
  };

  return (
    <div className={classes.root}>
      <div className={classes.title}>
        <b>{name}</b>
        <br />
        <i className={classes.desc}>{desc}</i>
      </div>
      {state === 0 ? (
        <Fab
          size="medium"
          disabled
          color="primary"
          aria-label="add"
          onClick={handleMinus}
        >
          <RemoveIcon />
        </Fab>
      ) : (
        <Fab
          size="medium"
          color="primary"
          aria-label="add"
          onClick={handleMinus}
        >
          <RemoveIcon />
        </Fab>
      )}
      {state}
      <Fab size="medium" color="primary" aria-label="add" onClick={handlePlus}>
        <AddIcon />
      </Fab>
    </div>
  );
}

export default AppFab;
