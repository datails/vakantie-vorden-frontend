import "date-fns";
import React, { useContext } from "react";
import { Grid } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
//@ts-ignore
import DateFnsUtils from "@date-io/date-fns";
import dutchLocale from "date-fns/locale/nl";
import { useRouter } from "next/navigation";

import { addDays } from "date-fns";
import AppButton from "../app-button";
import { AppPersons } from "./components";
import { Context } from "../../store/store";
import { parseDateOrString } from "../../utils";
import useStyles from "./app-search.styles";

import { MobileDatePicker } from "@mui/x-date-pickers";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";

import type { ResponsiveStyleValue } from "@mui/system";
import type { GridDirection } from "@mui/material/Grid";

type Props = {
  showSearch?: boolean;
  direction?: ResponsiveStyleValue<GridDirection>;
};

function AppSearch({ showSearch = true, direction = "row" }: Props) {
  const date = new Date();
  const classes = useStyles();
  const [state, dispatch] = useContext<any>(Context);
  const router = useRouter();

  let checkin: Date =
    new Date(parseDateOrString(state.search.checkin)) || new Date();
  let checkout: Date =
    new Date(parseDateOrString(state.search.checkout)) || new Date();

  if (checkin < date) {
    dispatch({
      type: "SET_SEARCH",
      search: {
        checkin: addDays(date, 1).toISOString(),
        checkout: addDays(date, 2).toISOString(),
      },
    });
  }

  if (checkout < checkin) {
    dispatch({
      type: "SET_SEARCH",
      search: {
        checkout: addDays(checkin, 1).toISOString(),
      },
    });
  }

  const handleSearch = (search: { checkin: Date; checkout: Date }) => {
    const parsedSearch = JSON.parse(JSON.stringify(search));

    dispatch({
      type: "SET_SEARCH",
      search: parsedSearch,
    });

    router.push(
      "/vakantiehuisjes",
      //  false,
      {
        ...state.search,
        ...parsedSearch,
        checkin: parsedSearch.checkin.split("T")[0],
        checkout: parsedSearch.checkout.split("T")[0],
      }
    );
  };

  const handleCheckInChange = (date: Date | undefined | null) => {
    dispatch({
      type: "SET_SEARCH",
      search: {
        checkin: date,
      },
    });
  };

  const handleCheckoutChange = (date: Date | undefined | null) => {
    dispatch({
      type: "SET_SEARCH",
      search: {
        checkout: date,
      },
    });
  };

  return (
    <LocalizationProvider
      dateAdapter={AdapterDateFns}
      adapterLocale={dutchLocale}
    >
      <Grid
        container
        justifyContent="space-between"
        direction={direction}
        className={classes.container}
        spacing={10}
        component={"nav"}
      >
        <Grid className={classes.content}>
          <MobileDatePicker
            minDate={date}
            // margin="normal"
            // id="startDate"
            label="Inchecken"
            format="MM/dd/yyyy"
            value={checkin || date}
            onChange={handleCheckInChange}
            // title={"Inchecken"}
            // cancelLabel={"Annuleren"}
            className={classes.datePicker}
            slotProps={{ textField: { variant: "standard" } }}
          />
          {direction === "row" && <div className={classes.borderRight}></div>}
        </Grid>
        <Grid className={classes.content}>
          {checkin && (
            <MobileDatePicker
              minDate={checkin || date}
              // margin="normal"
              // id="endDate"
              label="Uitchecken"
              format="MM/dd/yyyy"
              value={checkout || date}
              onChange={handleCheckoutChange}
              // title={"Uitchecken"}
              // cancelLabel={"Annuleren"}
              className={classes.datePicker}
              slotProps={{ textField: { variant: "standard" } }}
            />
          )}
          {direction === "row" && <div className={classes.borderRight}></div>}
        </Grid>
        <Grid className={classes.content + classes.datePicker}>
          <AppPersons />
        </Grid>
        {showSearch && (
          <>
            <Grid className={classes.content}>
              {state.search.adults > 0 ? (
                <AppButton
                  handler={() =>
                    handleSearch({
                      checkin: state?.search?.checkin || date,
                      checkout: state?.search?.checkout || date,
                    })
                  }
                  styles={{
                    borderRadius: "100%",
                    width: "50px",
                    height: "50px",
                    minWidth: "50px",
                    backgroundColor: "#095e4b",
                  }}
                >
                  <SearchIcon />
                </AppButton>
              ) : (
                <AppButton
                  disabled
                  handler={() =>
                    handleSearch({
                      checkin: state?.search?.checkin || date,
                      checkout: state?.search?.checkout || date,
                    })
                  }
                  styles={{
                    borderRadius: "100%",
                    width: "50px",
                    height: "50px",
                    minWidth: "50px",
                  }}
                >
                  <SearchIcon />
                </AppButton>
              )}
            </Grid>
          </>
        )}
      </Grid>
    </LocalizationProvider>
  );
}

export default AppSearch;
