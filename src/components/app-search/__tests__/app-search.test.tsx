import React from "react";
import { render, fireEvent } from "@testing-library/react";
import Store from "../../../store/store";
import AppSearch from "../index";

test("renders AppSearch component", async () => {
  const { getByLabelText, getByTestId } = render(
    <Store>
      <AppSearch />
    </Store>
  );

  const checkinInput = getByLabelText("Inchecken");
  fireEvent.click(checkinInput);
  fireEvent.change(checkinInput, { target: { value: "01/20/2024" } });
  fireEvent.click(checkinInput);

  const checkoutInput = getByLabelText("Uitchecken");
  fireEvent.click(checkoutInput);
  fireEvent.change(checkoutInput, { target: { value: "01/25/2024" } });
  fireEvent.click(checkoutInput);
});
