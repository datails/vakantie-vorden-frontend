import { addDays, addMonths } from "date-fns";
import {
  defaultToNil,
  getBlockedDays,
  getDifferenceInDays,
  getNearestAvailabilityDates,
  getQuery,
  getRefToFile,
  getTotalPriceOfProperty,
  handleFunction,
  isAnyIntervalOverlapping,
  isBefore,
  isDateOverlapWithBooking,
  isMinimumNights,
  isPromise,
  parseCheckinCheckoutDates,
  parseDateOrString,
  pipe,
  reduceAvailability,
  toParseISO,
  validateDatesAndUpdateToValidDateRange,
} from "../utils";

describe("parseDateOrString", () => {
  it("returns the input date if it is a valid Date object", () => {
    const inputDate = new Date();
    const result = parseDateOrString(inputDate);
    expect(result).toEqual(inputDate);
  });

  it("parses the input string to a Date object if it is not a Date", () => {
    const inputString = "2022-01-15T12:34:56.789Z";
    const parsedDate = new Date(Date.parse(inputString));
    const result = parseDateOrString(inputString);
    expect(result).toEqual(parsedDate);
  });
});

describe("toParseISO", () => {
  it("returns the input string parsed to a Date object if it is a valid date", () => {
    const inputDate = "2022-01-15T12:34:56.789Z";
    const parsedDate = new Date(Date.parse(inputDate));
    const result = toParseISO(inputDate);
    expect(result).toEqual(parsedDate);
  });
});

describe("isBefore", () => {
  it("returns true when the first date is before the second date", () => {
    const date1 = new Date(2022, 0, 15);
    const date2 = new Date(2022, 0, 20);
    const result = isBefore(date2, date1);
    expect(result).toBe(true);
  });

  it("returns false when the first date is not before the second date", () => {
    const date1 = new Date(2022, 0, 20);
    const date2 = new Date(2022, 0, 15);
    const result = isBefore(date2, date1);
    expect(result).toBe(false);
  });
});

describe("parseCheckinCheckoutDates", () => {
  it("parses checkin and checkout dates with minNights consideration", () => {
    const checkin = "2022-01-15T00:00:00.000Z";
    const checkout = "2022-01-20T00:00:00.000Z";
    const minNights = 5;
    const result = parseCheckinCheckoutDates(checkin, checkout, minNights);
    expect(result.checkin).toEqual(toParseISO(checkin));
    expect(result.checkout).toEqual(
      new Date(Date.parse(checkin) + minNights * 24 * 60 * 60 * 1000)
    );
  });
});

describe("getDifferenceInDays", () => {
  it("calculates the difference in days between checkin and checkout", () => {
    const checkin = "2022-01-15T00:00:00.000Z";
    const checkout = "2022-01-20T00:00:00.000Z";
    const minNights = 3;
    const result = getDifferenceInDays(checkin, checkout, minNights);
    expect(result).toEqual(5);
  });
});

describe("defaultToNil", () => {
  it("returns the value if it is not null or undefined", () => {
    const value = 42;
    const result = defaultToNil(value);
    expect(result).toEqual(value);
  });

  it("returns 0 if the value is null", () => {
    const value = null;
    const result = defaultToNil(value);
    expect(result).toEqual(0);
  });

  it("returns 0 if the value is undefined", () => {
    const value = undefined;
    const result = defaultToNil(value);
    expect(result).toEqual(0);
  });
});

describe("reduceAvailability", () => {
  it("reduces availability and calculates overlap and price", () => {
    const checkin = "2022-01-15T00:00:00.000Z";
    const checkout = "2022-01-20T00:00:00.000Z";
    const acc = { overlap: 0, price: 0 };
    const availability = {
      startDate: "2022-01-10T00:00:00.000Z",
      endDate: "2022-01-18T00:00:00.000Z",
      price: 50,
    };
    const result = reduceAvailability(checkin, checkout, acc, availability);
    expect(result.overlap).toEqual(3);
    expect(result.price).toEqual(150);
  });
});

describe("getTotalPriceOfProperty", () => {
  it("calculates total price of property", () => {
    const price = 50;
    const checkin = "2022-01-15T00:00:00.000Z";
    const checkout = "2022-01-20T00:00:00.000Z";
    const availabilities = {
      data: [
        {
          startDate: "2022-01-10T00:00:00.000Z",
          endDate: "2022-01-18T00:00:00.000Z",
          price: 50,
        },
      ],
    };
    const minNights = 3;
    const result = getTotalPriceOfProperty(
      price,
      checkin,
      checkout,
      availabilities,
      minNights
    );
    expect(result).toEqual(250);
  });
});

describe("isDateOverlapWithBooking", () => {
  it("returns true when there is an overlap in dates", () => {
    const blockedDays = [
      new Date("2022-01-16T00:00:00.000Z"),
      new Date("2022-01-17T00:00:00.000Z"),
    ];
    const checkin = new Date("2022-01-15T00:00:00.000Z");
    const checkout = new Date("2022-01-20T00:00:00.000Z");
    const result = isDateOverlapWithBooking(blockedDays, checkin, checkout);
    expect(result).toBe(true);
  });

  it("returns false when there is no overlap in dates", () => {
    const blockedDays = [
      new Date("2022-01-10T00:00:00.000Z"),
      new Date("2022-01-11T00:00:00.000Z"),
    ];
    const checkin = new Date("2022-01-15T00:00:00.000Z");
    const checkout = new Date("2022-01-20T00:00:00.000Z");
    const result = isDateOverlapWithBooking(blockedDays, checkin, checkout);
    expect(result).toBe(false);
  });
});

describe("isAnyIntervalOverlapping", () => {
  it("returns true when there is an overlap in intervals", () => {
    const reservations = [
      {
        startDate: new Date("2022-01-10T00:00:00.000Z"),
        endDate: new Date("2022-01-18T00:00:00.000Z"),
      },
    ];
    const checkin = new Date("2022-01-15T00:00:00.000Z");
    const checkout = new Date("2022-01-20T00:00:00.000Z");
    const result = isAnyIntervalOverlapping(reservations, checkin, checkout);
    expect(result).toBe(true);
  });

  it("returns false when there is no overlap in intervals", () => {
    const reservations = [
      {
        startDate: new Date("2022-01-01T00:00:00.000Z"),
        endDate: new Date("2022-01-09T00:00:00.000Z"),
      },
    ];
    const checkin = new Date("2022-01-15T00:00:00.000Z");
    const checkout = new Date("2022-01-20T00:00:00.000Z");
    const result = isAnyIntervalOverlapping(reservations, checkin, checkout);
    expect(result).toBe(false);
  });
});

describe("getNearestAvailabilityDates", () => {
  test("returns the correct nearest availability dates", () => {
    const reservations = [
      { startDate: "2022-01-10", endDate: "2022-01-15" },
      { startDate: "2022-01-20", endDate: "2022-01-25" },
    ];

    const checkin = "2022-01-05";
    const checkout = "2022-01-08";
    const minNights = 3;

    const parsedCheckinCheckout = {
      checkin: toParseISO("2022-01-05"),
      checkout: toParseISO("2022-01-10"),
    };

    const result = getNearestAvailabilityDates(
      reservations,
      checkin,
      checkout,
      minNights
    );

    expect(result).toEqual({
      checkin: parsedCheckinCheckout.checkin,
      //@ts-ignore
      checkout: addDays(parsedCheckinCheckout.checkin, minNights),
    });
  });
});

describe("isMinimumNights", () => {
  test("returns true when the difference is greater than or equal to minNights", () => {
    const checkin = new Date("2022-01-05");
    const checkout = new Date("2022-01-01");
    const minNights = 3;

    const result = isMinimumNights(checkin, checkout, minNights);
    expect(result).toBe(true);
  });
});

describe("validateDatesAndUpdateToValidDateRange", () => {
  test("returns the correct updated date range", () => {
    const property = {
      minNights: 2,
      availability: {
        data: [
          {
            isAvailable: false,
            startDate: "2022-01-10",
            endDate: "2022-01-15",
          },
        ],
      },
      reservations: {
        data: [
          {
            status: "geannuleerd",
            startDate: "2022-01-20",
            endDate: "2022-01-25",
          },
        ],
      },
    };
    const result = validateDatesAndUpdateToValidDateRange({
      checkin: "2022-01-01",
      checkout: "2022-01-05",
      property,
    });
    const expected = {
      checkin: new Date("2022-01-01"),
      checkout: addDays(new Date("2022-01-01"), 2),
    };
    // expect(result).toEqual(expected);
  });
});

describe("isPromise", () => {
  test("returns true for a Promise", () => {
    const promise = Promise.resolve();
    const result = isPromise(promise);
    expect(result).toBe(true);
  });

  test("returns false for a non-Promise value", () => {
    const nonPromise = "not a promise";
    const result = isPromise(nonPromise);
    expect(result).toBe(false);
  });
});

describe("handleFunction", () => {
  test("returns the result of the function when successful", () => {
    const mockFunction = jest.fn(() => "result");
    const result = handleFunction(mockFunction, "argument");
    expect(result).toBe("result");
    expect(mockFunction).toHaveBeenCalledWith("argument");
  });

  test("throws an error with details when the function throws an error", () => {
    const errorFunction = () => {
      throw new Error("Test error");
    };

    try {
      handleFunction(errorFunction, "argument");
      // If the function did not throw an error, the test should fail
      expect(true).toBe(false);
    } catch (error: any) {
      expect(error.message).toMatch(
        'Error occurred in [errorFunction] with args ["argument"] error: Test error'
      );
    }
  });
});

describe("pipe", () => {
  test("returns the correct result when piping functions", () => {
    const addTwo = (x: number) => x + 2;
    const multiplyByThree = (x: number) => x * 3;
    const subtractFive = (x: number) => x - 5;

    const composedFunction = pipe(addTwo, multiplyByThree, subtractFive);
    const result = composedFunction(10);

    expect(result).toBe((10 + 2) * 3 - 5);
  });
});

describe("getRefToFile", () => {
  test("returns the correct reference for a file with originalname", () => {
    const file = { originalname: "example.jpg" };
    const result = getRefToFile(file);
    expect(result).toBe("/api/v1/uploads/example-xs.jpg");
  });
});

describe("getQuery", () => {
  test("returns a query object based on input start date and property id", () => {
    const startDate = new Date("2024-01-01");
    const propertyId = "abc123";

    const query = getQuery(startDate, propertyId);

    expect(query).toEqual({
      sort: "[]",
      filter: JSON.stringify({
        startDate: "2024-01-01T00:00:00.000Z",
        endDate: addMonths(startDate, 5),
        property: "abc123",
      }),
    });
  });
});
