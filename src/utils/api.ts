import { stringify } from "query-string";
import * as U from "./utils";

import { appConfig } from "../config/appConfig";

type RequestOptions = {
  method?: string;
  headers?: { [key: string]: string };
  body?: string;
};

const httpGet = async (path: string, options: RequestOptions = {}) => {
  try {
    const res = await fetch(`${appConfig.backendHost}${path}`, {
      headers: {
        "x-portal-id": appConfig.portalId,
        ...options.headers,
      },
      ...options,
    });

    if (!res.ok) {
      const errorResponse = await (typeof res.json === "function"
        ? res.json()
        : res.text());

      console.error(errorResponse);

      throw new Error("HTTP error");
    }

    const { data } = await res.json();

    return data;
  } catch (error) {
    console.error(error);
    throw new Error("Failed to fetch data");
  }
};

export const loadSchemas = () => httpGet("/api/v1/public/property-schema");
export const loadProperties = () => httpGet("/api/v1/public/property-list");

export const loadReservation = (id: string | string[] | undefined) =>
  httpGet(`/api/v1/public/reservation/${id}`);
export const loadProperty = ({ id }: { id: string | string[] | undefined }) =>
  httpGet(`/api/v1/public/property/${id}`);
export const loadPortal = (id: string) =>
  httpGet(`/api/v1/public/portal/${id}`);

export const loadOwner = async (owner: string) => {
  try {
    const data = await httpGet(`/api/v1/public/owner/${owner}`);

    return {
      ...data,
      file: {
        ...data.file,
        originalname: U.getRefToFile(data.file),
      },
    };
  } catch (error) {
    console.error(error);
    throw new Error("Failed to load owner");
  }
};

export async function loadAvailability({
  startDate = new Date(),
  id,
}: {
  startDate?: Date;
  id: string | string[] | undefined;
}) {
  try {
    const query = U.getQuery(startDate, id);
    const data = await httpGet(
      `/api/v1/public/availability?${stringify(query)}`
    );
    const blockedDays = U.getBlockedDays(data, U.isAvailability);
    const availabilities = U.getBlockedDays(data);

    return {
      availabilities,
      data,
      blockedDays,
    };
  } catch (error) {
    console.error(error);
    throw new Error("Failed to load availability");
  }
}

export async function loadReservations({
  startDate = new Date(),
  id,
}: {
  startDate?: Date;
  id: string | string[] | undefined;
}) {
  try {
    const query = U.getQuery(startDate, id);
    const data = await httpGet(
      `/api/v1/public/reservations?${stringify(query)}`
    );
    const blockedDays = U.getBlockedDays(data);

    return {
      blockedDays,
      data: data,
    };
  } catch (error) {
    console.error(error);
    throw new Error("Failed to load reservations");
  }
}

export async function loadPropertyWithReservations({
  id,
  startDate = new Date(),
}: {
  id: string | string[] | undefined;
  startDate?: Date;
}) {
  try {
    const [property, reservations, availability] = await Promise.all([
      loadProperty({ id }),
      loadReservations({
        id,
        startDate,
      }),
      loadAvailability({
        id,
        startDate,
      }),
    ]);

    return {
      ...property,
      blockedDays: [...reservations?.blockedDays, ...availability?.blockedDays],
      reservations,
      availability,
    };
  } catch (error) {
    console.error(error);
    throw new Error("Failed to load property with reservations");
  }
}

export async function createReservation({
  reservation,
  recaptchaValue,
  client,
}: {
  reservation: any;
  recaptchaValue: string;
  client: any;
}) {
  try {
    const price = U.getTotalPriceOfProperty(
      reservation.property.price,
      U.parseDateOrString(reservation.startDate || new Date()).toISOString(),
      U.parseDateOrString(reservation.endDate || new Date()).toISOString(),
      reservation.property.availability?.data ||
        reservation.property.availability,
      reservation.property.minNights
    );

    return httpGet(`/api/v1/public/create-reservation`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        "x-portal-id": appConfig.portalId,
      },
      body: JSON.stringify({
        client: {
          ...client,
          housenumber: parseInt(client.housenumber),
        },
        reservation: {
          ...reservation,
          property: reservation.property.id,
          price,
        },
        recaptchaValue,
      }),
    });
  } catch (error) {
    console.error(error);
    throw new Error("Failed to create reservation");
  }
}

export async function createClientAndReservation({
  client,
  reservation,
  recaptchaValue,
}: {
  client: any;
  reservation: any;
  recaptchaValue: string;
}) {
  try {
    const reservationResp = await createReservation({
      reservation: {
        ...reservation,
        recaptchaValue,
      },
      recaptchaValue,
      client,
    });

    return {
      client: reservationResp?.client,
      reservation: reservationResp?.reservation,
      error:
        Boolean(reservationResp?.message) &&
        Boolean(reservationResp?.statusCode),
      errorMessage: reservationResp?.message,
    };
  } catch (error) {
    console.error(error);
    throw new Error("Failed to create client and reservation");
  }
}
