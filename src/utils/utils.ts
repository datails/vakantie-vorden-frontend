import * as R from "ramda";
import * as D from "date-fns";
import * as moment from "moment";
import { extendMoment } from "moment-range";
import "moment/locale/nl";
import type { Moment } from "moment";

const rangeMoment = extendMoment(moment);

export const parseDateOrString = R.pipe(
  R.defaultTo(D.addDays(new Date(), 1)),
  R.unless<Date, Date>(R.is(Date), (value: unknown) =>
    D.parseISO(value as string)
  )
);

export const toParseISO = R.unless(D.isValid, D.parseISO);

export const isBefore = R.flip(R.curry(D.isBefore));

export const parseCheckinCheckoutDates = R.curryN(
  3,
  function (checkin, checkout, minNights) {
    return R.applySpec({
      checkin: R.pipe(R.always(checkin), toParseISO),
      checkout: R.pipe(
        R.always(checkout),
        toParseISO,
        R.when(isBefore(checkin), R.always(D.addDays(checkin, minNights)))
      ),
    })({});
  }
);

export const getDifferenceInDays = (
  checkin: any,
  checkout: any,
  minNights: any
) => {
  const dates = parseCheckinCheckoutDates(checkin, checkout, minNights);
  if (dates.checkin instanceof Date && dates.checkout instanceof Date)
    return D.differenceInDays(dates.checkout, dates.checkin);

  return 0;
};

export const defaultToNil = R.defaultTo(0);

export const reduceAvailability = R.curryN(
  4,
  function (
    checkin: any,
    checkout: any,
    acc: { overlap?: number; price?: number },
    availability: { startDate: any; endDate: any; price: number }
  ) {
    const start = parseDateOrString(availability.startDate);
    const end = parseDateOrString(availability.endDate);

    const isCheckinBeforeStartDate = checkin <= start;

    const overlap = D.getOverlappingDaysInIntervals(
      {
        start: isCheckinBeforeStartDate ? parseDateOrString(checkin) : start,
        end: isCheckinBeforeStartDate ? parseDateOrString(checkout) : end,
      },
      {
        start: isCheckinBeforeStartDate ? start : parseDateOrString(checkin),
        end: isCheckinBeforeStartDate ? end : parseDateOrString(checkout),
      }
    );

    return {
      overlap: defaultToNil(acc.overlap) + overlap,
      price: defaultToNil(acc.price) + overlap * availability.price,
    };
  }
);

export const getTotalPriceOfProperty = (
  price: number,
  checkin: any,
  checkout: any,
  availabilities: any,
  minNights: any
) => {
  const totalDays = getDifferenceInDays(checkin, checkout, minNights);
  const computedPrice = (
    Array.isArray(availabilities?.data)
      ? availabilities.data
      : Array.isArray(availabilities)
      ? availabilities
      : []
  ).reduce(reduceAvailability(checkin, checkout), {});

  return (
    (totalDays - defaultToNil(computedPrice.overlap)) * defaultToNil(price) +
    defaultToNil(computedPrice.price)
  );
};

export const isDateOverlapWithBooking = (
  blockedDays: any[],
  checkin: any,
  checkout: any
) => {
  return blockedDays.some((blockedDay) =>
    D.isWithinInterval(blockedDay, {
      start: checkin,
      end: checkout,
    })
  );
};

export const isAnyIntervalOverlapping = (
  reservations: any[],
  checkin: any,
  checkout: any
) => {
  return reservations.some((reservation) => {
    const start = parseDateOrString(reservation.startDate);
    const end = parseDateOrString(reservation.endDate);
    const isCheckinBeforeStartDate = D.isBefore(checkin, start);

    return D.areIntervalsOverlapping(
      {
        start: isCheckinBeforeStartDate ? checkin : start,
        end: isCheckinBeforeStartDate ? checkout : end,
      },
      {
        start: isCheckinBeforeStartDate ? start : checkin,
        end: isCheckinBeforeStartDate ? end : checkout,
      }
    );
  });
};

export function getNearestAvailabilityDates(
  reservations: any[],
  checkin: string,
  checkout: string,
  minNights: number
) {
  const parsedCheckinCheckout = parseCheckinCheckoutDates(
    checkin,
    checkout,
    minNights
  );
  let i = 0;
  let start = parsedCheckinCheckout.checkin;
  let end = parsedCheckinCheckout.checkout;

  while (isAnyIntervalOverlapping(reservations, start, end)) {
    //@ts-ignore
    start = D.addDays(parsedCheckinCheckout.checkin, i);
    //@ts-ignore
    end = D.addDays(parsedCheckinCheckout.checkout, i + minNights);

    i++;
  }

  return {
    checkin: start,
    checkout: end,
  };
}

export function isMinimumNights(checkin: any, checkout: any, minNights: any) {
  return D.differenceInDays(checkin, checkout) >= minNights;
}

export function validateDatesAndUpdateToValidDateRange({
  checkin,
  checkout,
  property,
}: {
  checkin: string;
  checkout: string;
  property: {
    availability?: { data?: any[] };
    reservations?: { data?: any[] };
    minNights: number;
  };
}) {
  const date = new Date();

  // update the dates, as we have moment and date-fns throughtout the project
  // the calendar uses moment, but our api uses date-fns
  let checkinDate = parseDateOrString(checkin) || date;
  let checkoutDate = parseDateOrString(checkout) || date;

  // if checkout is before checkin, update both
  if (D.isBefore(checkoutDate, checkinDate)) {
    checkin = D.addDays(date, 1).toISOString();
    checkout = D.addDays(D.parseISO(checkin), property.minNights).toISOString();
  }

  // if checkin is after, update checkout
  if (D.isAfter(checkinDate, checkoutDate)) {
    checkout = D.addDays(
      parseDateOrString(checkin),
      property.minNights
    ).toISOString();
  }

  if (!isMinimumNights(checkin, checkout, property.minNights)) {
    checkout = D.addDays(
      parseDateOrString(checkin),
      property.minNights
    ).toISOString();
  }

  // get updated date range
  const dateRange = getNearestAvailabilityDates(
    [
      ...(property.availability?.data?.filter?.(
        (avble) => !avble.isAvailable
      ) || []),
      ...(property.reservations?.data?.filter?.(
        (rsv) => rsv.status !== "geannuleerd"
      ) || []),
    ],
    checkin,
    checkout,
    property.minNights
  );

  return {
    checkin: dateRange.checkin,
    checkout: dateRange.checkout,
  };
}

export function isPromise(value: any) {
  return Boolean(value && value instanceof Promise);
}

export const handleFunction = (fn: Function, arg: any) => {
  try {
    return fn(arg);
  } catch (error: any) {
    throw new Error(
      `Error occurred in [${fn.name || fn}] with args [${JSON.stringify(
        arg
      )}] error: ${error.message}`
    );
  }
};

export const pipe = (...fns: Function[]) => {
  return (param: any) => {
    return fns.reduce((prevResult, currentFunction) => {
      return isPromise(prevResult)
        ? // eslint-disable-next-line no-console
          prevResult
            .then((promiseResult: any) =>
              handleFunction(currentFunction, promiseResult)
            )
            .catch(console.error)
        : handleFunction(currentFunction, prevResult);
    }, param);
  };
};

export const getRefToFile = (file: { originalname?: string }) => {
  if (!file) {
    return "/anonymous_user.jpeg";
  }

  const [name, ext] = file.originalname?.split?.(".") || [];
  return `/api/v1/uploads/${name}-xs.${ext}`;
};

export const isAvailability = (date: { isAvailable: boolean }) =>
  !date.isAvailable;

export const getBlockedDays = (
  data: any[],
  fn: (date: any) => boolean = () => true
) => {
  let blockedDays: any[] = [];

  if (Array.isArray(data)) {
    for (const date of data) {
      const range = rangeMoment.range(date.startDate, date.endDate);
      const acc = Array.from(range.by("day"));

      if (acc.length && fn(date)) {
        blockedDays = blockedDays.concat(acc);
      }
    }
  }

  return blockedDays;
};

export const getQuery = (
  startDate: Date,
  id: string | string[] | undefined
) => ({
  sort: JSON.stringify([]),
  filter: JSON.stringify({
    startDate,
    endDate: D.addMonths(startDate, 5),
    property: id,
  }),
});
