import React, { SyntheticEvent } from "react";
import MuiAlert, { AlertProps } from "@mui/material/Alert";
import { Snackbar, SnackbarCloseReason } from "@mui/material";

import makeStyles from "@mui/styles/makeStyles";

const useStyles = makeStyles((theme: any) => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
}));

function Alert(props: AlertProps) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

type Props = {
  message?: string;
  // severity: any;
  handler: (
    _event: Event | SyntheticEvent<any, Event>,
    reason: SnackbarCloseReason
  ) => void;
  open: boolean;
};
export default function AppSnackBar({
  message,
  // severity,
  handler,
  open,
}: Props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Snackbar
        open={open}
        autoHideDuration={6000}
        onClose={handler}
        message={message}
      >
        {/* <Alert onClose={handler} severity={severity}>
          {message}
        </Alert> */}
      </Snackbar>
    </div>
  );
}
