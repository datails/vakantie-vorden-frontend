import React, { useContext } from "react";
import {
  Card,
  Grid,
  CardActions,
  CardContent,
  CardMedia,
  Typography,
} from "@mui/material";
import Button from "@mui/material/Button";
import WifiIcon from "@mui/icons-material/Wifi";
import PetsIcon from "@mui/icons-material/Pets";
import AccessibleIcon from "@mui/icons-material/Accessible";
import PeopleIcon from "@mui/icons-material/People";
//@ts-ignore
import classnames from "classnames";
import { differenceInCalendarDays } from "date-fns";

import useStyles from "./app-catalogue-item.styles";

import { Context } from "../../../../store/store";
import { parseDateOrString } from "../../../../utils";
import type { Moment } from "moment";

type Props = {
  item: {
    _id: string;
    price: number;
    beds: number;
    persons: number;
    isHandicapped: boolean;
    isFitForAnimals: boolean;
    hasWifi: boolean;
    city: string;
    title: string;
    images: [string];
  };
};

export default function AppCatalogueListItem({ item }: Props) {
  const classes = useStyles();
  const [state] = useContext<any>(Context);

  const checkout: any = parseDateOrString(state.search.checkout);
  const checkin: any = parseDateOrString(state.search.checkin);
  const days = differenceInCalendarDays(checkout, checkin);

  return (
    <Card className={classes.card}>
      <Grid
        container
        justifyContent="space-around"
        className={classes.container}
        spacing={10}
      >
        <Grid item className={classes.content}>
          <CardMedia
            component="img"
            alt={item.title}
            height="140"
            image={item.images.find((image) => image)}
            title={item.title}
            className={classes.media}
          />
          <Grid
            container
            justifyContent="space-around"
            className={classes.container}
            spacing={10}
          >
            <Grid item xs={12} className={classes.content}>
              <CardContent
                className={classnames(
                  classes.content,
                  classes.padding,
                  classes.flexDirectionColumn
                )}
              >
                <Typography
                  gutterBottom
                  variant="h5"
                  component="h2"
                  className={classes.title}
                >
                  {item.title}
                </Typography>
                <Typography
                  variant="body2"
                  // color="textSecondary"
                  component="p"
                  color="primary"
                >
                  {item.city}
                </Typography>
                <Typography
                  variant="body2"
                  // color="textSecondary"
                  component="p"
                  className={classes.vertAlign}
                  color="secondary"
                >
                  {item.hasWifi && <WifiIcon className={classes.smallIcons} />}
                  {item.isFitForAnimals && (
                    <PetsIcon className={classes.smallIcons} />
                  )}
                  {item.isHandicapped && (
                    <AccessibleIcon className={classes.smallIcons} />
                  )}
                  {<PeopleIcon className={classes.smallIcons} />}
                  <b className={classes.smallIcons}>{item.persons}</b> personen
                  - <b className={classes.smallIcons}>{item.beds}</b> bedden
                </Typography>
              </CardContent>
            </Grid>
            <Grid
              item
              xs={12}
              className={classnames(classes.content, classes.backgroundLight)}
            >
              <CardActions className={classes.cardActions}>
                <Typography
                  gutterBottom
                  // variant="body"
                  variant="body1"
                  component="p"
                  className={classes.vertAlign}
                >
                  <span className={classes.cta}>{days}</span> dagen -{" "}
                  {new Intl.NumberFormat("nl-NL", {
                    style: "currency",
                    currency: "EUR",
                  }).format(days * item.price)}
                </Typography>
                <Typography
                  gutterBottom
                  variant="body2"
                  component="p"
                  // color="secondary"
                >
                  <b>Per nacht</b>
                  <br />
                  <b>
                    {new Intl.NumberFormat("nl-NL", {
                      style: "currency",
                      currency: "EUR",
                    }).format(item.price)}
                  </b>
                </Typography>
                <Button
                  className={classes.button}
                  href={`/vakantiehuisjes/${item._id}`}
                  color="primary"
                >
                  Bekijk
                </Button>
              </CardActions>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Card>
  );
}
