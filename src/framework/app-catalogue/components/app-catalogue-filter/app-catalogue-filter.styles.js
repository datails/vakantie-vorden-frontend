import makeStyles from '@mui/styles/makeStyles';
import createStyles from '@mui/styles/createStyles';

export default makeStyles((theme) =>
  createStyles({
    sideSearch: {
      border: `1px solid ${theme.palette.primary.light}`,
      display: "flex",
      flexDirection: "column",
      padding: "25px",
      borderRadius: "25px",
    },
    formControl: {
      display: "flex",
      width: "100%",
    },
    sideBarTitle: {
      marginBottom: "30px",
      fontWeight: 500,
    },
  })
);
