import React, { ChangeEvent } from "react";
//@ts-ignore
import classNames from "classnames";
import {
  FormGroup,
  FormControlLabel,
  Checkbox,
  Paper,
  Typography,
} from "@mui/material";
import StickyBox from "react-sticky-box";

import { AppAccordion } from "../../../../components";

import { translate } from "../../app-catalogue.content";
import useStyles from "./app-catalogue-filter.styles";

type Props = {
  schemas: { [key: string]: { [key: string]: boolean } };
  services: { [key: string]: boolean };
  setServices: (event: ChangeEvent<HTMLInputElement>, checked: boolean) => void;
};

function AppCatalogueFilter({ schemas, services, setServices }: Props) {
  const classes = useStyles();

  return (
    <StickyBox offsetTop={80} offsetBottom={20}>
      <Paper className={classes.sideSearch} id="search-container">
        <Typography
          gutterBottom
          variant="h5"
          component="h2"
          className={classes.sideBarTitle}
        >
          Verfijn zoekopdracht
        </Typography>
        <FormGroup row>
          {Object.keys(schemas).map((key, index) => {
            if (key.startsWith("has") && index < 5) {
              return (
                <FormControlLabel
                  className={classes.formControl}
                  control={
                    <Checkbox
                      checked={services[key]}
                      onChange={setServices}
                      name={key}
                    />
                  }
                  //@ts-ignore
                  label={translate[key]}
                />
              );
            }
            return null;
          })}
          <AppAccordion>
            {Object.keys(schemas).map((key, index) => {
              if (key.startsWith("has") && index >= 5) {
                return (
                  <FormControlLabel
                    className={classNames(classes.formControl)}
                    control={
                      <Checkbox
                        checked={services[key]}
                        onChange={setServices}
                        name={key}
                      />
                    }
                    //@ts-ignore
                    label={translate[key]}
                  />
                );
              }
              return null;
            })}
          </AppAccordion>
        </FormGroup>
      </Paper>
    </StickyBox>
  );
}

export default AppCatalogueFilter;
