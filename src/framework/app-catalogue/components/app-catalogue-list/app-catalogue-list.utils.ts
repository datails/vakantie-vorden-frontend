export function isBabies(
  property: { isFitForBabies: boolean },
  state: { search: { babies: number } }
) {
  return state.search.babies ? property.isFitForBabies : true;
}

export function isMaxGuest(
  property: { persons: number },
  state: { search: { adults: number; children: number } }
) {
  return property.persons >= state.search.adults + state.search.children;
}

export function isAllServicesOnProperty(
  property: { [key: string]: boolean },
  services: { [key: string]: boolean }
) {
  for (const name in services) {
    if (services[name] === true && property[name] !== services[name]) {
      return false;
    }
  }

  return true;
}
