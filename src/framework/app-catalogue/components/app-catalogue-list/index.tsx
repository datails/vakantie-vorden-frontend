import React, { useContext } from "react";
import { Grid, Typography } from "@mui/material";

import { Context } from "../../../../store/store";
import AppCatalogueItem from "../app-catalogue-item";

import useStyles from "./app-catalogue-list.styles";
import * as utils from "./app-catalogue-list.utils";

type Property = {
  _id: string;
  price: number;
  beds: number;
  persons: number;
  isHandicapped: boolean;
  isFitForAnimals: boolean;
  hasWifi: boolean;
  city: string;
  title: string;
  images: [string];
  isFitForBabies: boolean;
};

type Props = {
  services: { [key: string]: boolean };
  properties: Property[];
};

function AppCatalogueList({ services, properties }: Props) {
  const classes = useStyles();
  const [state] = useContext<any>(Context);

  const filterOnSearch = properties.filter(
    (property: { isFitForBabies: boolean; persons: number }) =>
      utils.isBabies(property, state) && utils.isMaxGuest(property, state)
  );
  const filterOnServices = filterOnSearch.filter((property: any) =>
    utils.isAllServicesOnProperty(property, services)
  );

  return (
    <Grid
      container
      justifyContent="space-around"
      className={classes.list}
      spacing={10}
    >
      {filterOnServices.length === 0 ? (
        <Typography
          gutterBottom
          variant="h5"
          component="h2"
          className={classes.notFoundTitle}
        >
          Geen woningen gevonden!
        </Typography>
      ) : (
        filterOnServices.map((item) => {
          return (
            <Grid item xs={12} className={classes.listCatalogue}>
              <AppCatalogueItem item={item} />
            </Grid>
          );
        })
      )}
    </Grid>
  );
}

export default AppCatalogueList;
