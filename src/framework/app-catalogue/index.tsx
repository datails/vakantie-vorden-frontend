import React, { ChangeEvent, useState } from "react";
import { Grid, Paper } from "@mui/material";
import { AppCirculairLoader, AppSearch } from "../../components";
import { AppCatalogueList, AppCatalogueFilter } from "./components";
import useStyles from "./app-catalogue.styles";
import AppLoader from "../../components/app-loader";

type Props = {
  schemas: { [key: string]: { [key: string]: boolean } };
  properties: [
    {
      _id: string;
      price: number;
      beds: number;
      persons: number;
      isHandicapped: boolean;
      isFitForAnimals: boolean;
      hasWifi: boolean;
      city: string;
      title: string;
      images: [string];
      isFitForBabies: boolean;
    }
  ];
};

function AppCatalogue({ schemas, properties }: Props) {
  const classes = useStyles();

  const [services, setServices] = useState({});

  const handleServiceChange = (event: ChangeEvent<HTMLInputElement>) => {
    setServices({ ...services, [event.target.name]: event.target.checked });
  };

  if (!schemas || !properties)
    return (
      <AppLoader
      //  visible={true}
      />
    );

  return (
    <Grid
      container
      justifyContent="space-around"
      className={classes.container}
      spacing={10}
    >
      <Grid item sm={12} md={4} className={classes.content}>
        <AppCatalogueFilter
          schemas={schemas}
          services={services}
          setServices={handleServiceChange}
        />
      </Grid>
      <Grid item sm={12} md={8} className={classes.content}>
        <Paper className={classes.searchBar}>
          <AppSearch showSearch={false} />
        </Paper>
        {!properties ? (
          <AppCirculairLoader />
        ) : (
          <AppCatalogueList properties={properties} services={services} />
        )}
      </Grid>
    </Grid>
  );
}

export default AppCatalogue;
