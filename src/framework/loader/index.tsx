import React, { SyntheticEvent, useContext, useState } from "react";
import { Backdrop, CircularProgress, SnackbarCloseReason } from "@mui/material";
import makeStyles from "@mui/styles/makeStyles";
import AppSnackbar from "../snackbar";
import { Context } from "../../store/store";

const useStyles = makeStyles((theme: any) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 999999,
    color: "#fff",
    flexDirection: "column",
  },
}));

type Props = {
  message?: string;
  severity?: string;
  visible?: boolean;
};

function AppLoader({ message, severity, visible }: Props) {
  const classes = useStyles();
  const [state] = useContext<any>(Context);
  const [snackBar, setSnackBar] = useState({
    open: true,
  });

  const _handleClose = (
    _event: Event | SyntheticEvent<any, Event>,
    reason: SnackbarCloseReason
  ) => {
    if (reason === "clickaway") {
      return;
    }

    setSnackBar({
      open: false,
    });
  };

  return (
    <div>
      <Backdrop className={classes.backdrop} open={visible || state.loader}>
        <CircularProgress color="inherit" size={65} />
        <AppSnackbar
          message={message}
          // severity={severity}
          handler={_handleClose}
          open={snackBar.open}
        />
      </Backdrop>
    </div>
  );
}

export default AppLoader;
