import makeStyles from '@mui/styles/makeStyles';

export default makeStyles((theme) => ({
  backdrop: {
    zIndex: theme.zIndex.drawer + 999999,
    color: "#fff",
    flexDirection: "column",
  },
}));
