import React from "react";
import { Grid, Link, Typography } from "@mui/material";
//@ts-ignore
import classNames from "classnames";
import useStyles from "./footer.styles";

function Footer() {
  const classes = useStyles();

  return (
    <footer>
      <Grid
        container
        className={classes.container}
        justifyContent="space-around"
      >
        <Grid item xs={12} sm={12} md={4} lg={4} className={classes.content}>
          <Typography variant="h2" gutterBottom className={classes.title}>
            Vakantiehuisjes Vorden
          </Typography>
          <Typography variant="body1" gutterBottom className={classes.text}>
            Vind het vakantiehuisjes in Vorden dat bij je past! Op zoek naar een
            rustig vakantiehuisjes, een chalet in de bossen of een Bed and
            Breakfast in de Achterhoek? Vind het bij Vakantiehuisje Vorden.
          </Typography>
        </Grid>
        <Grid item xs={12} sm={12} md={4} lg={4} className={classes.content}>
          <Typography variant="h2" gutterBottom className={classes.title}>
            Contact
          </Typography>
          <Typography variant="body1" className={classes.anchor}>
            <Link
              href="mailto:elena.kuyper@gmail.com"
              target="_blank"
              color="inherit"
              underline="hover"
            >
              Stuur een email
            </Link>
          </Typography>
        </Grid>
        <Grid item xs={12} sm={12} md={4} lg={4} className={classes.content}>
          <Typography variant="h2" gutterBottom className={classes.title}>
            Locatie
          </Typography>
          <iframe
            width="100%"
            height="350"
            frameBorder={0}
            scrolling="no"
            marginHeight={0}
            marginWidth={0}
            src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=en&amp;q=Heijendaalseweg%202a,%20Vorden+(Bed%20and%20Breakfast%20De%20Schuilplaats)&amp;t=&amp;z=15&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"
          ></iframe>
        </Grid>
      </Grid>
      <Grid
        container
        className={classNames([classes.container, classes.backgroundMain])}
        justifyContent="space-around"
      >
        <Grid item xs={12} className={classes.content}>
          <Typography variant="body1" className={classes.anchor}>
            <Link
              href="//datails.nl"
              target="_blank"
              color="inherit"
              className={classes.colorLight}
              underline="hover"
            >
              Powered By DATAILS.
            </Link>
          </Typography>
        </Grid>
      </Grid>
    </footer>
  );
}

export default Footer;
