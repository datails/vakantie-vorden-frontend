import React from "react";
import { render, screen } from "@testing-library/react";
import Footer from "../index";

test("Correct text displayed", () => {
  render(<Footer />);
  const footerText = screen.getByText(/Stuur een email/i);
  expect(footerText.textContent).toBe("Stuur een email");
});
