import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";

export default makeStyles((theme) =>
  createStyles({
    anchor: {
      display: "flex",
      flexDirection: "column",
      fontSize: "1rem",
      lineHeight: 2,
      fontFamily:
        "'Comfortaa','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif",
      fontWeight: 300,
    },
    container: {
      background: "#ebefdf",
      margin: "0",
      width: "100%",
      padding: "40px",
      [theme.breakpoints?.down("md")]: {
        padding: "40px 0",
      },
    },
    content: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "flex-start",
      padding: "40px",
    },
    image: {
      margin: "35px 0",
      width: "100%",
    },
    margin: {
      margin: theme.spacing(1),
    },
    title: {
      fontSize: "1rem !important",
      fontFamily:
        "'Comfortaa','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif",
      fontWeight: 500,
      marginBottom: "1.5rem",
    },
    text: {
      fontSize: "1rem",
      lineHeight: 1.2,
      fontFamily:
        "'Comfortaa','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif",
      fontWeight: 300,
      textAlign: "justify",
    },
    backgroundMain: {
      padding: "0",
      textAlign: "center",
      background: "rgb(82, 87, 72)",
    },
    colorLight: {
      color: "#FFF",
      fontWeight: 700,
    },
  })
);
