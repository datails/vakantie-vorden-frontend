import React from "react";
import { Grid, Breadcrumbs, Typography, Link } from "@mui/material";
import NavigateNextIcon from "@mui/icons-material/NavigateNext";
import { useRouter } from "next/router";
import useStyles from "./breadcrumb.styles";

type Props = {
  backgroundColor: string;
  current?: string;
};

function AppBreadCrumbs({ backgroundColor, current }: Props) {
  const classes = useStyles();
  const router = useRouter();

  const pathnames = router.asPath.split("/").filter((x) => x);

  return (
    <Grid
      container
      className={classes.container}
      justifyContent="space-around"
      style={{ backgroundColor: "rgb(82, 87, 72)" }}
    >
      <Grid item xs={12} className={classes.content}>
        <Breadcrumbs
          aria-label="breadcrumb"
          separator={
            <NavigateNextIcon fontSize="small" className={classes.colorWhite} />
          }
        >
          <Link
            className={classes.colorWhite}
            color="#fff"
            href="/"
            underline="hover"
          >
            Home
          </Link>
          {pathnames.map((value, index) => {
            const last = index === pathnames.length - 1;
            const to = `/${pathnames.slice(0, index + 1).join("/")}`;

            return last ? (
              <Typography
                color="textPrimary"
                key={to}
                className={classes.colorWhite}
              >
                {current ? current : to.split("/")?.[1]}
              </Typography>
            ) : (
              <Link
                color="inherit"
                href={to}
                key={to}
                className={classes.colorWhite}
                underline="hover"
              >
                {to.split("/")?.[1]}
              </Link>
            );
          })}
        </Breadcrumbs>
      </Grid>
    </Grid>
  );
}

export default AppBreadCrumbs;
