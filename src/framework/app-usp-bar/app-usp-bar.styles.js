import createStyles from "@mui/styles/createStyles";
import makeStyles from "@mui/styles/makeStyles";

export default makeStyles((theme) =>
  createStyles({
    container: {
      margin: "0",
      width: "100%",
      padding: "80px 40px",
      background: "#FFF",
      [theme.breakpoints?.down("md")]: {
        padding: "40px 0",
      },
    },
    content: {
      display: "flex",
      justifyContent: "center",
    },
    title: {
      marginBottom: "50px",
    },
  })
);
