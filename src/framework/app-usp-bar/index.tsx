import React from "react";
import { Grid, Typography } from "@mui/material";
import AppUSPCard from "../../components/app-usp-bar";
import AppButton from "../../components/app-button";
import useStyles from "./app-usp-bar.styles";

type Props = {
  title?: string;
  backgroundColor?: string;
};

export default function ComponentTopBanner({ title, backgroundColor }: Props) {
  const classes = useStyles();

  return (
    <Grid
      container
      className={classes.container}
      justifyContent="space-evenly"
      spacing={10}
      style={{ backgroundColor: backgroundColor }}
    >
      <Grid item lg={12} xl={8} justifyContent="center" textAlign={"center"}>
        <Grid container spacing={10} justifyContent="space-evenly">
          <Grid item sm={12} justifyContent="center" textAlign={"center"}>
            <Typography
              variant="h2"
              component="h2"
              gutterBottom
              className={classes.title}
            >
              {title || "Huren van vakantiewoningen van Vordenaren"}
            </Typography>
            <Typography variant="subtitle1" component="h3">
              Huren van woningen via vakantie-vorden biedt de mogelijkheid om
              direct
              <br />
              met de eigenaar in contact te komen zonder tussenkomst van derden.
              <br />
              Vakantie-vorden kenmerkt zich door het aanbieden van unieke
              woningen bij bekende aanbieders.
            </Typography>
          </Grid>
          <Grid item md={12} lg={6} className={classes.content}>
            <AppUSPCard
              background="de-schuilplaats-keuken.jpeg"
              desc="Vind de unieke vakantie accomodatie van een Vordenaar."
              title="Zoeken"
            >
              1
            </AppUSPCard>
          </Grid>
          <Grid item md={12} lg={6} className={classes.content}>
            <AppUSPCard
              background="de-schuilplaats-kachel.jpeg"
              desc="Bekijk of de woning beschikbaar is op de geselecteerde data en boek direct!"
              title="Boeken"
            >
              2
            </AppUSPCard>
          </Grid>
          <Grid item md={12} lg={6} className={classes.content}>
            <AppUSPCard
              background="de-schuilplaats-zitplek.jpeg"
              desc="Geniet van een heerlijke vakantie in de Achterhoek."
              title="Genieten"
            >
              3
            </AppUSPCard>
          </Grid>
          <Grid item sm={12} justifyContent="center" textAlign={"center"}>
            <AppButton
              styles={{
                minWidth: "250px",
                fontSize: "1.1rem",
                height: "60px",
              }}
              href="/vakantiehuisjes"
            >
              Huisjes zoeken
            </AppButton>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
}
