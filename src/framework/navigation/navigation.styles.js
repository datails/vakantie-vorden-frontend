import makeStyles from "@mui/styles/makeStyles";
import createStyles from "@mui/styles/createStyles";

export default makeStyles((theme) =>
  createStyles({
    colorDark: {
      color: "#000!important",
    },
    heartBeatIcon: {
      color: theme.palette.primary.main,
      fontSize: "2rem",
    },
    heartBeatIconAwake: {
      color: "#000",
    },
    menuButton: {
      color: theme.palette.primary.main,
      marginRight: "10px",
    },
    navigationBar: {
      zIndex: 100,
      position: "fixed",
      top: "-74px",
      transition: "transform 0.2s",
      transform: "translateY(74px)",
      background: "#FFF",
    },
    navigationBarHide: {
      transform: "translateY(0)",
    },
    navigationBarInitial: {
      backgroundColor: "transparent",
      boxShadow: "none",
    },
    navigationBarInner: {
      zIndex: 1,
      justifyContent: "space-between",
      maxWidth: 1200,
      width: "100%",
      margin: "0 auto",
      padding: "0 20px",
    },
    offset: {
      height: "74px",
    },
    offset0: {
      height: 0,
    },
    root: {
      flexGrow: 1,
      maxHeight: "64px",
    },
    title: {
      fontFamily:
        "'Comfortaa','HelveticaNeue','Helvetica Neue',Helvetica,Arial,sans-serif",
      fontWeight: 500,
      color: theme.palette.primary.main,
    },
    titleAwake: {
      color: "#000",
    },
    logo: {
      display: "flex",
      height: "50px",
      justifySelf: "center",
      alignSelf: "center",
    },
    colorWhite: {
      color: "#FFF",
    },
    colorDark: {
      color: theme.palette.primary.main,
    },
    hiddenMdUp: {
      [theme.breakpoints.up("md")]: {
        display: "none !important",
      },
    },
    hiddenMdDown: {
      [theme.breakpoints?.down("md")]: {
        display: "none !important",
      },
    },
    navList: {
      color: "#095e4b",
      display: "flex",
      minWidth: 250,
      whiteSpace: "nowrap",
    },
    displayNone: {
      display: "none !important",
    },
  })
);
