import React, { useState, useEffect, useContext } from "react";
import { AppBar, Toolbar, IconButton } from "@mui/material";
import MenuIcon from "@mui/icons-material/Menu";
import PersonIcon from "@mui/icons-material/AccountCircle";
//@ts-ignore
import classNames from "classnames";

import { Context } from "../../store/store";
import useStyles from "./navigation.styles";
import img from "../../assets/logo.svg";
import Image from "next/image";

function AppNavigation() {
  const classes = useStyles();

  const [state, dispatch] = useContext<any>(Context);
  const [atTopOfPage, setAtTopOfPage] = useState<boolean>(true);
  const [sleep, setSleep] = useState<boolean>(false);

  const handleScroll = () => {
    setAtTopOfPage(window.pageYOffset < 275 ? true : false);
    setSleep(window.pageYOffset < 350 ? true : false);
  };

  // show or hide the drawer
  const toggleDrawer = () => {
    dispatch({
      type: "TOGGLE_DRAWER",
    });
  };

  // component did unmount
  useEffect(() => {
    return () => window.removeEventListener("scroll", handleScroll);
  }, []);

  // component did mount
  useEffect(() => window.addEventListener("scroll", handleScroll), []);

  return (
    <div className={classes.root}>
      <AppBar
        color="default"
        position="relative"
        className={
          atTopOfPage
            ? classes.navigationBarInitial
            : sleep
            ? classNames(classes.navigationBar, classes.navigationBarHide)
            : classes.navigationBar
        }
      >
        <Toolbar className={classes.navigationBarInner}>
          <a
            href="/"
            className={
              atTopOfPage
                ? classes.heartBeatIcon
                : classNames(classes.heartBeatIcon, classes.heartBeatIconAwake)
            }
          >
            <Image
              className={classes.logo}
              alt="Logo Vakantie Vorden"
              src={img}
              height={50}
            />
          </a>
          <div>
            <IconButton
              edge="start"
              className={
                atTopOfPage
                  ? classNames(classes.menuButton, classes.hiddenMdUp)
                  : classNames(classes.menuButton, classes.colorDark)
              }
              color="inherit"
              aria-label="menu"
              onClick={toggleDrawer}
              size="large"
            >
              <MenuIcon
                className={atTopOfPage ? classes.colorWhite : classes.colorDark}
              />
            </IconButton>
            {/* <IconButton
              edge="start"
              className={
                atTopOfPage
                  ? classes.menuButton
                  : classNames(classes.menuButton, classes.colorDark)
              }
              color="inherit"
              aria-label="menu"
              href={`${window.location.protocol}//admin.${window.location.hostname}`}
            >
              <PersonIcon
                className={atTopOfPage ? classes.colorWhite : classes.colorDark}
              />
            </IconButton> */}
          </div>
        </Toolbar>
      </AppBar>
      <div className={atTopOfPage ? classes.offset0 : classes.offset}></div>
    </div>
  );
}

export default AppNavigation;
