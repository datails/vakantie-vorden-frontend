import React, { createContext, useReducer } from "react";
import Reducer from "./reducer";

let initialState = {
  drawer: false,
  loader: false,
  client: {},
  search: {
    adults: 0,
    babies: 0,
    children: 0,
    guests: 0,
    checkin: new Date(),
    chekout: new Date(),
  },
};

if (typeof window !== "undefined") {
  const storedState = window.localStorage.getItem("root:state");
  if (storedState) {
    initialState = JSON.parse(storedState);
  }
}

const Store = ({ children }) => {
  const [state, dispatch] = useReducer(Reducer, initialState);
  return (
    <Context.Provider value={[state, dispatch]}>{children}</Context.Provider>
  );
};

export const Context = createContext(initialState);

export default Store;
