export const appConfig = {
  portalId: process.env.NEXT_PUBLIC_REACT_APP_PORTAL_ID || "",
  backendHost: process.env.NEXT_PUBLIC_REACT_APP_API_HOST || "",
};
