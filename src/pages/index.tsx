import React from "react";
import { Divider } from "@mui/material";
import Head from "next/head";
import AppTextArea from "../components/home/components/text-area";
import AppHeader from "../components/home/components/header";
import { GetStaticProps } from "next";

type Props = {
  data: {
    title: string;
    description: string;
    ogTitle: string;
    ogUrl: string;
  };
};

function AppHome({ data }: Props) {
  return (
    <React.Fragment>
      <Head>
        <title>{data.title}</title>
        <meta name="description" content={data.description} />
        <meta property="og:description" content={data.description} />
        <meta property="og:title" content={data.ogTitle} />
        <meta property="og:url" content={data.ogUrl} />
      </Head>
      <AppHeader />
      <Divider />
      <AppTextArea />
      <Divider />
    </React.Fragment>
  );
}

export const getStaticProps: GetStaticProps<Props> = async () => {
  const data = {
    title: "Vakantie Vorden | Vakantie huis in de bossen van Vorden.",
    description:
      "Een prachtig vakantiehuis in Vorden dat bij je past! Op zoek naar een rustige vakantiehuisje midden in het Vordense bos? Vind het bij Bed and Breakfast de Schuilplaats in Vorden.",
    ogTitle:
      "Vakantie Vorden | Vakantie huis in de bossen van Vorden.",
    ogUrl: "https://vakantie-vorden.nl/",
  };

  return {
    props: { data },
  };
};

export default AppHome;
