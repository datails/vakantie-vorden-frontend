import React from "react";
import Head from "next/head";
import { Divider } from "@mui/material";
import AppHeader from "../../components/404/components/header";
import AppText from "../../components/404/components/text";
import BreadCrumbs from "../../framework/breadcrumb";
import { GetStaticProps } from "next";

type Props = {
  data: {
    title: string;
    description: string;
    ogTitle: string;
    ogUrl: string;
    current: string;
  };
};

function AppReview({ data }: Props) {
  return (
    <React.Fragment>
      <Head>
        <title>{data.title}</title>
        <meta name="description" content={data.description} />
        <meta property="og:description" content={data.description} />
        <meta property="og:title" content={data.ogTitle} />
        <meta property="og:url" content={data.ogUrl} />
      </Head>
      <Divider />
      <AppHeader />
      <BreadCrumbs backgroundColor="#4092B8" current={data.current} />
      <Divider />
      <AppText />
      <Divider />
    </React.Fragment>
  );
}

export const getStaticProps: GetStaticProps<Props> = async () => {
  const data = {
    title: "404 not found",
    description: "Helaas, deze pagina hebben wij niet kunnen vinden!",
    ogTitle: "404 not found",
    ogUrl: "https://vakantie-vorden.nl/404",
    current: "404 niet gevonden",
  };

  return {
    props: { data },
  };
};

export default AppReview;
