import React from "react";
import Head from "next/head";
import { Divider } from "@mui/material";

import { AppHeader } from "../../components/properties/components";
import { AppCatalogue, AppBreadCrumb } from "../../framework";
import * as U from "../../utils";
import { GetStaticProps } from "next";

export const getStaticProps: GetStaticProps<Props> = async () => {
  const schemas = await U.loadSchemas();
  const properties = await U.loadProperties();

  return {
    props: {
      schemas,
      properties: properties?.data ?? properties,
    },
  };
};

type Props = {
  properties: [
    {
      _id: string;
      price: number;
      beds: number;
      persons: number;
      isHandicapped: boolean;
      isFitForAnimals: boolean;
      hasWifi: boolean;
      city: string;
      title: string;
      images: [string];
      isFitForBabies: boolean;
    }
  ];
  schemas: { [key: string]: { [key: string]: boolean } };
};

function AppVakantieHuisjes({ properties, schemas }: Props) {
  return (
    <React.Fragment>
      <Head>
        <title>Vakantiewoningen in Vorden</title>
        <meta
          name="description"
          content="Vind het vakantiehuis in Vorden dat bij u past! Kies uit bed-and-breakfast of complete vakantiewoningen."
        />
        <meta
          property="og:description"
          content="Vind het vakantiehuis in Vorden dat bij u past! Kies uit bed-and-breakfast of complete vakantiewoningen."
        />
        <meta property="og:title" content="Vakantiewoningen in Vorden" />
        <meta
          property="og:url"
          content={"https://vakantie-vorden.nl/vakantiehuisjes"}
        />
      </Head>
      <Divider />
      <AppHeader />
      <AppBreadCrumb backgroundColor="#4092B8" />
      <Divider />
      <AppCatalogue properties={properties} schemas={schemas} />
      <Divider />
    </React.Fragment>
  );
}

export default AppVakantieHuisjes;
