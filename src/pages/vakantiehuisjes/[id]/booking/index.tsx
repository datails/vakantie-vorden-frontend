import React, { useContext, useEffect, useState } from "react";
import Head from "next/head";
import { Divider, Grid, Typography } from "@mui/material";
import { AppHeader } from "../../../../components/booking/components";
import useStyles from "../../../../components/booking/booking.styles";
import {
  loadPropertyWithReservations,
  loadReservation,
} from "../../../../utils";
import { AppCirculairLoader } from "../../../../components";
import { useRouter } from "next/router";

type Property = {
  title: string;
};
type Reservation = {};

export default function Booking() {
  const router = useRouter();

  const classes = useStyles();

  const [property, setProperty] = useState<Property>();

  const [reservation, setReservation] = useState<Reservation>();

  const loadBooking = async () => {
    const resp = await loadReservation(router.query.bookingId);
    setReservation(resp);
  };

  useEffect(() => {
    if (router.query.bookingId) loadBooking();
  }, []);

  const loadProperty = async () => {
    const resp = await loadPropertyWithReservations({ id: router.query.id });
    setProperty(resp);
  };

  useEffect(() => {
    if (router.query.id) loadProperty();
  }, [router.query]);

  if (!property) return <AppCirculairLoader />;

  return (
    <React.Fragment>
      <Head>
        <title>Uw booking van vakantiewoning {property.title}</title>
        <meta
          name="description"
          content="Bekijk de reservering bij uw vakantiewoning."
        />
        <meta
          property="og:description"
          content="Bekijk de reservering bij uw vakantiewoning."
        />
        <meta
          property="og:title"
          content="Bekijk de reservering bij uw vakantiewoning."
        />
        <meta property="og:url" content={"https://vakantie-vorden.nl"} />
      </Head>
      <Divider />
      <AppHeader />
      <Divider />
      <Grid
        container
        className={classes.container}
        justifyContent="space-evenly"
        spacing={10}
      >
        <Grid item xs={12} md={8} justifyContent="center" textAlign={"center"}>
          <Typography color="textPrimary" component="h1" variant="h2">
            Bedankt voor uw booking!
          </Typography>
          <Typography color="textPrimary" className={classes.content}>
            Ter referentie, het booking id is {router.query.bookingId}. U kunt
            dit nummer gebruiken tijdens verdere communicatie. Ook zal u een
            bevestigingsemail ontvangen met daarin dezelfde informatie.
          </Typography>
          <Divider />
          <Typography color="textPrimary" className={classes.content}>
            <b>Let op:</b>
            <br /> Aan deze aanvraag kunnen geen rechten worden ontleend. De
            aanvraag dient bevestigd te worden door de verhuurder. Hier ontvangt
            u een email over.
          </Typography>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
