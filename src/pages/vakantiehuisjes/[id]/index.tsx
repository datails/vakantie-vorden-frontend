import React, { useCallback, useContext, useMemo, useState } from "react";
import { Grid, Divider } from "@mui/material";
import StickyBox from "react-sticky-box";
import { AppBreadCrumb } from "../../../framework";
import {
  AppProperty,
  AppPropertyHeader,
  AppAvailabilitySidebar,
} from "../../../components/property/components";

import usePropertyStyles from "../../../components/property/components/property.styles";
import { AppCirculairLoader } from "../../../components";
import { loadPropertyWithReservations } from "../../../utils";
import * as moment from "moment";
import { extendMoment } from "moment-range";
import "moment/locale/nl";
import { GetStaticPaths, GetStaticProps } from "next";
import type { Moment } from "moment";
import { useApi } from "../../../hooks/useApi";

// @ts-ignore
interface PropsProperty extends Property {
  blockedDays: string;
  availability: {
    availabilities: string;
    blockedDays: string;
  };
}

interface Property {
  title: string;
  subtitle: string;
  rules: string;
  hasWifi: boolean;
  checkin: Moment;
  isFitForAnimals: boolean;
  isSmokingProperty: boolean;
  url: string;
  id: string;
  files: {
    fileNames: string[];
  }[];
  persons: number;
  beds: number;
  minNights: number;
  squareMeters: number;
  description: string;
  price: number;
  numberOfDaysBeforeCancel: number;
  availability: {
    blockedDays: [Moment];
    data: [
      {
        isAvailable: boolean;
        startDate: string | number | Date;
        endDate: string | number | Date;
        price: number;
      }
    ];
  };
  blockedDays: [Moment];
  isVATIncluded: boolean;
  servicePrice: number;
}

type Props = {
  property: PropsProperty;
};

type StringProps = {
  property: PropsProperty;
};

function toMoment(dateList: string) {
  const rangeMoment = extendMoment(moment);
  return JSON.parse(dateList).map((momentObj: string) =>
    rangeMoment(new Date(momentObj))
  );
}

function toString(dateList: Moment[]) {
  return JSON.stringify(dateList);
}

const toPropsProperty = (property: Property): PropsProperty => {
  return {
    ...property,
    blockedDays: toString(property.blockedDays),
    availability: {
      ...property.availability,
      availabilities: toString((property.availability as any)?.availabilities || []),
      blockedDays: toString(property.availability.blockedDays),
    },
  };
}

const VakantieHuisContent = ({ property }: StringProps) => {
  const classes = usePropertyStyles();
  return (
      <main>
        <AppPropertyHeader />
        <AppBreadCrumb backgroundColor="#4092B8" current={property.title} />
        <Divider />
        <Grid
          container
          className={classes.container}
          justifyContent="space-evenly"
          spacing={10}
        >
          <Grid item xs={12} md={8} justifyContent="center" textAlign={"center"}>
            <AppProperty property={property as any} />
          </Grid>
          <Grid item xs={12} md={4} justifyContent="center" textAlign={"center"}>
            <StickyBox offsetTop={100} offsetBottom={20}>
              <AppAvailabilitySidebar property={property as any} />
            </StickyBox>
          </Grid>
        </Grid>
      </main>
    );
  }

const VakantieHuis = ({ property }: StringProps) => {
  const memoizedArgs = useMemo(() => [{ id: property.id }], [property.id]);
  const memoizedFn = useCallback(() => loadPropertyWithReservations(memoizedArgs[0]), [property.id]);

  const { data } = useApi(memoizedFn, memoizedArgs, [property.id]);

  const originalProperty = useMemo(() => ({
    ...property,
    blockedDays: toMoment(property.blockedDays),
    availability: {
      ...property.availability,
      availabilities: toMoment(property.availability.availabilities),
      blockedDays: toMoment(property.availability.blockedDays),
    },
  }), [property]);

  if (!data && !originalProperty) return <AppCirculairLoader />;

  // @ts-ignore
  return <VakantieHuisContent property={data || originalProperty} />;
};

export const getStaticProps: GetStaticProps<Props> = async ({ params }) => {
  const property = await loadPropertyWithReservations({ id: params?.id });

  return {
    props: {
      property: toPropsProperty(property),
    },
  };
};

type PathParams = {
  id: string;
};

export const getStaticPaths: GetStaticPaths<PathParams> = async () => {
  const paths = [
    { params: { id: "63b91f1f9285300022f3503d" } },
  ];

  return {
    paths,
    fallback: false,
  };
};

export default VakantieHuis;
