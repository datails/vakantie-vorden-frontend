import React from "react";
import Head from "next/head";
import { Divider, Grid } from "@mui/material";
import StickyBox from "react-sticky-box";

import {
  AppForm,
  AppHeader,
} from "../../../../components/reservations/components";
import useStyles from "../../../../components/reservations/reservations.styles";

import ReservationSidebar from "../../../../components/reservations/components/property-sidebar";
import { AppCirculairLoader } from "../../../../components";
import { loadPropertyWithReservations } from "../../../../utils";
import { AppBreadCrumb } from "../../../../framework";
import { GetStaticPaths, GetStaticProps } from "next";

type PathParams = {
  id: string;
};

export const getStaticPaths: GetStaticPaths<PathParams> = async () => {
  const paths = [
    { params: { id: "63b91f1f9285300022f3503d" } },
  ];

  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps<Props> = async ({ params }) => {
  const property = await loadPropertyWithReservations({ id: params?.id });
  return {
    props: {
      property: {
        ...property,
        blockedDays: JSON.stringify(property.blockedDays),
        availability: {
          ...property.availability,
          availabilities: JSON.stringify(property.availability.availabilities),
          blockedDays: JSON.stringify(property.availability.blockedDays),
        },
      },
    },
  };
};

type Props = {
  property: {
    id: number;
    title: string;
    persons: number;
    price: number;
    availability: {
      data: [
        {
          startDate: string | number | Date;
          endDate: string | number | Date;
          price: number;
        }
      ];
    };
    minNights: number;
    isVATIncluded: boolean;
    servicePrice: number;
  };
};

function AppReservations({ property }: Props) {
  const classes = useStyles();

  if (!property) {
    return <AppCirculairLoader />;
  }

  return (
    <React.Fragment>
      <Head>
        <title>Reserveer een vakantiewoning in Vorden</title>
        <meta
          name="description"
          content="Reserveer het vakantiehuis in Vorden dat bij u past! Reserveer bed-and-breakfast of complete vakantiewoningen."
        />
        <meta
          property="og:description"
          content="Reserveer het vakantiehuis in Vorden dat bij u past! Reserveer bed-and-breakfast of complete vakantiewoningen."
        />
        <meta
          property="og:title"
          content="Reserveer een vakantiewoning in Vorden"
        />
        <meta
          property="og:url"
          content={"https://vakantie-vorden.nl/reserveren"}
        />
      </Head>
      <Divider />
      <AppHeader />
      <AppBreadCrumb backgroundColor="#525748" current={property.title} />
      <Divider />
      <Grid
        container
        className={classes.container}
        justifyContent="space-evenly"
        spacing={10}
      >
        <Grid item xs={12} md={8} justifyContent="center" textAlign={"center"}>
          <AppForm />
        </Grid>
        <Grid item xs={12} md={4} justifyContent="center" textAlign={"center"}>
          <StickyBox offsetTop={100} offsetBottom={20}>
            <ReservationSidebar property={property} />
          </StickyBox>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}

export default AppReservations;
