import React, { useState, useEffect } from "react";
import { StyledEngineProvider, ThemeProvider } from "@mui/material/styles";
import CssBaseline from "@mui/material/CssBaseline";
import theme from "../theme/theme";
import Store from "../store/store";
import type { AppProps } from "next/app";
import { AppDrawer, AppFooter, AppLoader, AppNavigation } from "../framework";
import Head from "next/head";

function MyApp({ Component, pageProps }: AppProps) {
  const [stylesLoaded, setStylesLoaded] = useState(false);
  useEffect(() => {
    // Simulate loading time
    setTimeout(() => {
      setStylesLoaded(true);
    }, 0);
  }, []);

  return (
    <>
      <Head>
        <link rel="icon" href="/logo.ico" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      </Head>
      <StyledEngineProvider injectFirst>
        <CssBaseline />
        {stylesLoaded && (
          <ThemeProvider theme={theme}>
            <Store>
              <React.Fragment>
                <AppNavigation />
                <Component {...pageProps} />
                <AppLoader />
                <AppDrawer />
                <AppFooter />
              </React.Fragment>
            </Store>
          </ThemeProvider>
        )}
      </StyledEngineProvider>
    </>
  );
}

export default MyApp;
