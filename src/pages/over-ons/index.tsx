import React from "react";
import Head from "next/head";
import { Divider } from "@mui/material";

import { AppHeader, AppTextAreaBottom } from "../../components/about-us/components";
import { AppBreadCrumb } from "../../framework";

function OverOns() {
  return (
    <React.Fragment>
      <Head>
        <title>Over de verhuurders van Vakantie Vorden</title>
        <meta
          name="description"
          content="Als trotse inwoners van Vorden zetten wij ons in voor het aanbieden van kleinschalige vakantiewoningen en B&B's in de prachtige omgeving van Vorden."
        />
        <meta
          property="og:description"
          content="Als trotse inwoners van Vorden zetten wij ons in voor het aanbieden van kleinschalige vakantiewoningen en B&B's in de prachtige omgeving van Vorden."
        />
        <meta property="og:title" content="Over de verhuurders van Vakantie Vorden" />
        <meta
          property="og:url"
          content={"https://vakantie-vorden.nl/over-ons"}
        />
      </Head>
      <Divider />
      <AppHeader />
      <AppBreadCrumb backgroundColor="#4092B8" />
      <Divider />
      <AppTextAreaBottom />
      <Divider />
    </React.Fragment>
  );
}

export default OverOns;
